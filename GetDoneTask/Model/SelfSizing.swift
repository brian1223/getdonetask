//
//  SelfSizing.swift
//  GetDoneTask
//
//  Created by Thang Thoi Truong on 5/4/19.
//  Copyright © 2019 Thang Thoi Truong. All rights reserved.
//

import UIKit

class SelfSizing: UITableView {

    var maxHeight: CGFloat = UIScreen.main.bounds.size.height
    
    override func reloadData() {
        super.reloadData()
        self.invalidateIntrinsicContentSize()
        self.layoutIfNeeded()
    }
    
    override var intrinsicContentSize: CGSize {
        let height = min(contentSize.height, maxHeight)
        return CGSize(width: contentSize.width, height: height)
    }

}
