//  Created by Thang Thoi Truong on 5/27/19.
//  Copyright © 2019 Thang Thoi Truong. All rights reserved.
//


import UIKit
import CoreData

class ToDoCell: UITableViewCell {
    
    
    @IBOutlet weak var taskLable: UILabel!
   
    @IBOutlet weak var btnImageUndone: UIButton!
    
    @IBOutlet weak var timeLable: UILabel!
    
    @IBOutlet weak var projectLab: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var colorBtn: UIButton!
    
    var cellDelegate: YourCellDelegate?
    
    var done = Bool()
    
    var taskName = String()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(taskLb : String, doneCheck : Bool, time : String, project : String,date : String, hexString : String,important : Bool){
        if important {
            taskLable.text = "★ \(taskLb)"
        }else{
             taskLable.text = taskLb
        }
        taskName = taskLb
        done = doneCheck
        timeLable.text = time
        projectLab.text = project
        dateLabel.text = date
        colorBtn.backgroundColor = UIColor(hexString: hexString)
        
        buttonState()
    }
    
    @IBAction func btnTapped(_ sender: UITableView) {
        
        if done {
             btnImageUndone.setImage(#imageLiteral(resourceName: "done"), for: .normal)
            timeLable.alpha = 0.45
            projectLab.alpha = 0.45
             taskLable.alpha = 0.45
              dateLabel.alpha = 0.45
            done = false
           
           
        }else{
            btnImageUndone.setImage(#imageLiteral(resourceName: "undone"), for: .normal)
            taskLable.alpha = 1
            timeLable.alpha = 1
            projectLab.alpha = 1
            dateLabel.alpha = 1
            done = true
           
   
            
        }

         fetchAndSave()
   
         cellDelegate?.didPressButton(sender)
    }
    
    func buttonState(){
        
        if done {
           btnImageUndone.setImage(UIImage(named: "done"), for: .normal)
            taskLable.alpha = 0.45
            timeLable.alpha = 0.45
            projectLab.alpha = 0.45
              dateLabel.alpha = 0.45
        }else{
            btnImageUndone.setImage(UIImage(named: "undone"), for: .normal)
            taskLable.alpha = 1
            timeLable.alpha = 1
            projectLab.alpha = 1
              dateLabel.alpha = 1
        }
    }
    
    func fetchAndSave(){
       print("Save and fetch")
        let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext
        
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
            request.predicate = NSPredicate(format: "%K = %@", "name", "\(taskName)")
            request.returnsObjectsAsFaults = false
        do {
    
                let result = try context!.fetch(request)
                for data in result as! [Task] {
                    if done {
                       
                         data.done = false
                    }else{
                       
                        data.done = true
                    }
                   
                }
                
                try? context?.save()
                
                
            } catch {
                
                print("Failed")
            }
            
            
            
        
    }
    
}
