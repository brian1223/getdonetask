
//  Created by Thang Thoi Truong on 5/27/19.
//  Copyright © 2019 Thang Thoi Truong. All rights reserved.
//

import UIKit

class CircleCollectionViewCell: UICollectionViewCell {
  
    @IBOutlet weak var cellCheckMArk: UIButton!
    var done = false
    //Function  for select and deselect checkmark.
   
    
    @IBAction func checkMarkTapped(_ sender: Any) {
        if done{
            print("YEs")
            done = false
        }else {
            print("No")
            done = true
        }

    }
    
    
    func toggleState(){
    
    }
    
    override func draw(_ rect: CGRect) { //Your code should go here.
        super.draw(rect)
        self.layer.cornerRadius = self.frame.size.width / 4
        
        
    }
  
   
}
