
import UIKit

class EventAndToDoCell: UITableViewCell{

    
    var dayInFullString = String()
    
    @IBOutlet weak var dayLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var colorShowView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(dayLb : String, timeLb : String, nameEvent : String,hexString : String){
        
        dayLabel.text = dayLb
        timeLabel.text = timeLb
        nameLabel.text = nameEvent
        colorShowView.backgroundColor = UIColor(hexString: hexString)
    }
    
    

}
