//  Created by Thang Thoi Truong on 5/27/19.
//  Copyright © 2019 Thang Thoi Truong. All rights reserved.
//

import UIKit

class ProjectTableViewCell: UITableViewCell {

    @IBOutlet weak var colorShowView: UIView!
    
    @IBOutlet weak var projectNam: UILabel!
    
    @IBOutlet weak var dateLb: UILabel!
    @IBOutlet weak var timeLb: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func cofigureCell(date : String,time : String, projectName : String, colorString : String){
        dateLb.text = date
        timeLb.text = time
        projectNam.text = projectName
        colorShowView.backgroundColor = UIColor(hexString: colorString)
        
    }
}
