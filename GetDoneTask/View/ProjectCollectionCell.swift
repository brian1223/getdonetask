
//  Created by Thang Thoi Truong on 5/27/19.
//  Copyright © 2019 Thang Thoi Truong. All rights reserved.
//

import UIKit
import ChameleonFramework

class ProjectCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var labelProject: UILabel!
    
    
    @IBOutlet weak var numberOfTask: UILabel!
    
    
    @IBOutlet weak var progressView: UIProgressView!


    @IBOutlet weak var dueDateLb: UILabel!
    
    @IBOutlet weak var percentDone: UILabel!
    
    let proressValue = 0
    
    override func awakeFromNib() {
    
        
    }
    
   
    
    
    func cofigureCell(project : String,numberOfTaskString : String,progress : Float, dueDate : String){
        
        
        labelProject.text = project
        numberOfTask.text = numberOfTaskString
        percentDone.text = "\(Int(progress * 100))%"
        progressView.progress = progress
        dueDateLb.text = dueDate
        
    }
    
    func configureColorCell(color : UIColor){
      
        percentDone.textColor = color
        progressView.progressTintColor = color
        labelProject.textColor = color
        dueDateLb.textColor = color
        numberOfTask.textColor = color
    }
}
