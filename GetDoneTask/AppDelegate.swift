//
//  AppDelegate.swift
//  GetDoneTask
//
//  Created by Thang Thoi Truong on 5/27/19.
//  Copyright © 2019 Thang Thoi Truong. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    let notificationCenter = UNUserNotificationCenter.current()
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter
    }()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
      
        IQKeyboardManager.shared.enableAutoToolbar = false
        
        
        notificationCenter.delegate = self
        
        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
        
        notificationCenter.requestAuthorization(options: options) {
            (didAllow, error) in
            if !didAllow {
                print("User has declined notifications")
            }
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "GetDoneTask")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}




extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if response.notification.request.identifier == "Local Notification" {
            print("Handling notifications with the Local Notification Identifier")
        }
        
        completionHandler()
    }
    
    func scheduleNotification(title: String,body : String,date : Date,identifier : String, triggerStyle : String) {
        
        let content = UNMutableNotificationContent() // Содержимое уведомления
        let categoryIdentifire = "Delete Notification Type"
        
        content.title = title
        content.body = body
        content.sound = UNNotificationSound(named: UNNotificationSoundName("Ascending.mp3"))
        content.badge = 1
        content.categoryIdentifier = categoryIdentifire
        
        
         let dateToChoose = date
        
        if triggerStyle == "daily" {
            let triggerDaily = Calendar.current.dateComponents([.hour, .minute, .second], from: date)
            let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDaily, repeats: true)
             createAfterTrigger(identifier: identifier, trigger: trigger, categoryIdentifire: categoryIdentifire, content: content)
        }else if triggerStyle == "monthly" {
            
            let triggerMonthly = Calendar.current.dateComponents([.day, .hour, .minute, .second], from: date)
            let trigger = UNCalendarNotificationTrigger(dateMatching: triggerMonthly, repeats: true)
             createAfterTrigger(identifier: identifier, trigger: trigger, categoryIdentifire: categoryIdentifire, content: content)
        }else if triggerStyle == "weekly" {
            
            let triggerWeekly = Calendar.current.dateComponents([.weekday, .hour, .minute, .second], from: date)
            let trigger = UNCalendarNotificationTrigger(dateMatching: triggerWeekly, repeats: true)
             createAfterTrigger(identifier: identifier, trigger: trigger, categoryIdentifire: categoryIdentifire, content: content)
            
        }else if triggerStyle == "dateSpecific" {
            let triggerDate = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: dateToChoose)
            let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
            createAfterTrigger(identifier: identifier, trigger: trigger, categoryIdentifire: categoryIdentifire, content: content)
        }
        
        
       
     
    }
    
    func createAfterTrigger(identifier : String, trigger : UNNotificationTrigger, categoryIdentifire : String,content : UNMutableNotificationContent){
        let identifier = identifier
        
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        notificationCenter.add(request) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
        
        let snoozeAction = UNNotificationAction(identifier: "Snooze", title: "Snooze", options: [])
        let deleteAction = UNNotificationAction(identifier: "DeleteAction", title: "Delete", options: [.destructive])
        let category = UNNotificationCategory(identifier: categoryIdentifire,
                                              actions: [snoozeAction, deleteAction],
                                              intentIdentifiers: [],
                                              options: [])
        
        notificationCenter.setNotificationCategories([category])
    }
    
    
    func stopLocalNotification(identifier : String){
        let center = UNUserNotificationCenter.current()
//        center.removeAllPendingNotificationRequests()
        // or you can remove specifical notification:
         center.removePendingNotificationRequests(withIdentifiers: [identifier])
    }
    
    // Cancel local notification
    
    
//    func cancelLocalNotification(UNIQUE_ID: String){
//
//        var notifyCancel = UILocalNotification()
//        var notifyArray = UIApplication.sharedApplication().scheduledLocalNotifications
//
//        for notifyCancel in notifyArray as! [UILocalNotification]{
//
//            let info: [String: String] = notifyCancel.userInfo as! [String: String]
//
//            if info[uniqueId] == uniqueId{
//
//                UIApplication.sharedApplication().cancelLocalNotification(notifyCancel)
//            }else{
//
//                println("No Local Notification Found!")
//            }
//        }
//    }
}
