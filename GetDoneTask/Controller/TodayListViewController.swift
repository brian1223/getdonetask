//
//  TodayListViewController.swift
//  GetDoneTask
//
//  Created by Thang Thoi Truong on 8/6/19.
//  Copyright © 2019 Thang Thoi Truong. All rights reserved.
//

import UIKit
import CoreData
import SCLAlertView
import NotificationBannerSwift
import ChameleonFramework
import TwicketSegmentedControl
import SPStorkController

class TodayListViewController: UIViewController {

    @IBOutlet weak var swipeLb: UILabel!
    
    @IBOutlet weak var twicketView: TwicketSegmentedControl!
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter
    }()
    
    let date = Date()
    
    var i = 0
    
    var segmentindex = 0
    
    var todayList = [Task]()
    
    var taskName = [String]()
    
    var upComingList = [Task]()
   
    var dateForUpcoming = [String]()
    
    var timeForToday = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
          let titles = ["Today","UpComing Plan"]
        twicketView.setSegmentItems(titles)
        twicketView.delegate = self
   
        self.view.setGradientBackgroundColor(colorOne:FlatBlue(), colorTwo: FlatPurple())
        self.twicketView.setGradientBackgroundColor(colorOne:FlatBlue(), colorTwo: FlatPurple())
        swipeLb.backgroundColor = UIColor.white
        
    }
    
    @IBAction func addButtonTapped(_ sender: Any) {
        if segmentindex == 0{
             presentAddVC(atIndex: 2, today: true)
        }else{
             presentAddVC(atIndex: 2, today: false)
        }
        
    }
    
    
    func presentAddVC(atIndex : Int,today : Bool){
        guard let addVC = storyboard?.instantiateViewController(withIdentifier: "AddVC") as? AddVC else{ return}
        
        
        let transitionDelegate = SPStorkTransitioningDelegate()
        
        transitionDelegate.storkDelegate = self
        
        addVC.transitioningDelegate = transitionDelegate
        
        addVC.modalPresentationStyle = .custom
        
        
        switch atIndex {
        case 0:
            addVC.showSwitch = 0
            addVC.color = FlatBlue()
            
        case 1:
            addVC.showSwitch = 1
            addVC.color = FlatGreen()
            
        case 2:
            addVC.showSwitch = 2
            addVC.color = FlatRed()
            
            if today {
                addVC.dateSelected = dateFormatter.string(from: Date())
                
            }
        default:
            addVC.color = FlatGreen()
        }
        
        self.present(addVC, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(self.wasDragged(gestureRecognizer:)))
        swipeLb.addGestureRecognizer(gesture)
        
        AppendToTodayAndUpcoming()
        
        displaySwipeLabel()
       
    }
    
    func displaySwipeLabel(){
        if segmentindex == 0{
            if todayList.count == 0 {
                swipeLb.text = "🔥 \n Action is the foundational key to all success."
               
            }else {
                
                appendToTaskName()
                
                let taskNameReal = taskName[0]
                let taskTime = timeForToday[0]
               
                swipeLb.text = "\(taskNameReal) \n \(taskTime)"
            }
        }else{
            if upComingList.count == 0 {
                swipeLb.text = " 👏🏻 \n Add Some More"
                
            }else {
                
                appendToTaskName()
                
                let taskNameReal = taskName[0]
                let taskDate = dateForUpcoming[0]
                swipeLb.text = "\(taskNameReal) \n \(taskDate)"
            }
        }
    }
    
    func appendToTaskName(){
         var i = 0
        if segmentindex == 0 {
            while i < todayList.count{
                
                let task = todayList[i]
                guard let takName = task.name else {return}
                guard let taskTime = task.time else {return}
                
                timeForToday.append(taskTime)
                taskName.append(takName)
                
                i += 1
                
            }
        }else{
            while i < upComingList.count{
                
                let task = upComingList[i]
                guard let takName = task.name else {return}
                guard let taskDate = task.date  else {return}
                
                dateForUpcoming.append(taskDate)
                taskName.append(takName)
                
                i += 1
                
            }
        }
       
    }
    
    func removeTaskName(){
        taskName.removeAll()
        todayList.removeAll()
        upComingList.removeAll()
        timeForToday.removeAll()
        dateForUpcoming.removeAll()
    }
    
    
    func bannerDone(){
        
        let banner = NotificationBanner(title: "Done",
                                        subtitle: "",
                                        style: .success,
                                        colors: FlatGreen() as? BannerColorsProtocol)
        banner.show(queuePosition: .front, bannerPosition: .bottom, queue: .default, on: self)
        
    }
    
    func bannerLater(upcoming : Bool){
        if upcoming {
            let banner = NotificationBanner(title: "Add 1 day to the task",
                                            subtitle: "",
                                            style: .warning,
                                            colors: FlatRed() as? BannerColorsProtocol)
            banner.show(queuePosition: .front, bannerPosition: .bottom, queue: .default, on: self)
        }else{
            let banner = NotificationBanner(title: "Add 10 more minutes to the task",
                                            subtitle: "",
                                            style: .warning,
                                            colors: FlatRed() as? BannerColorsProtocol)
              banner.show(queuePosition: .front, bannerPosition: .bottom, queue: .default, on: self)
        }
       
      
        
    }
    

    @objc func wasDragged(gestureRecognizer : UIPanGestureRecognizer){
       
        let labelPoint = gestureRecognizer.translation(in: view)
        
        
        swipeLb.center = CGPoint(x: view.frame.width/2 + labelPoint.x, y: view.frame.height/2 + labelPoint.y)
        
        let xFromCenter = view.bounds.width/2 - swipeLb.center.x
        
        
        var rotation = CGAffineTransform(rotationAngle: xFromCenter / 200)
        
        let scale =  min (100 / abs(xFromCenter), 1)
        
        var scaledAndRotated = rotation.scaledBy(x: scale, y: scale)
       
        swipeLb.transform = scaledAndRotated
        
       
        if gestureRecognizer.state == .ended {
            if swipeLb.center.x < (self.view.bounds.width/2 - 100){
                
                if segmentindex == 0{
                    if todayList.count == 0 {
                        
                    }else{
                        if i == taskName.count - 1 {
                            i = 0
                        }else{
                            
                            
                            i += 1
                            
                        }
                        fetchTask(nameTask: taskName[i], later: true, upcoming: false)
                        removeTaskName()
                        AppendToTodayAndUpcoming()
                        appendToTaskName()
                        bannerLater(upcoming: false)
                        
                    }
                }else{
                    if upComingList.count == 0 {
                        
                    }else{
                        if i == taskName.count - 1 {
                            i = 0
                        }else{
                            
                            
                            i += 1
                            
                        }
                        fetchTask(nameTask: taskName[i], later: true, upcoming: true)
                        removeTaskName()
                        AppendToTodayAndUpcoming()
                        appendToTaskName()
                        bannerLater(upcoming: true)
                        
                    }
                }
             
            
            }
            if swipeLb.center.x > (self.view.bounds.width/2 + 100){
                if segmentindex == 0 {
                    if todayList.count == 0 {
                        
                    }else{
                        if i == taskName.count - 1 {
                            i = 0
                        }else{
                            
                            
                        }
                        
                        fetchTask(nameTask: taskName[i], later: false, upcoming: false)
                        removeTaskName()
                        AppendToTodayAndUpcoming()
                        appendToTaskName()
                        bannerDone()
                    }
                }else{
                    if upComingList.count == 0 {
                        
                    }else{
                        if i == taskName.count - 1 {
                            i = 0
                        }else{
                            
                            
                        }
                        
                        fetchTask(nameTask: taskName[i], later: false, upcoming: false)
                        removeTaskName()
                        AppendToTodayAndUpcoming()
                        appendToTaskName()
                        bannerDone()
                    }
                }
                
                
           
            }
       
            
            if segmentindex == 0 {
                
                if taskName.count == 0 {
                  swipeLb.text = "🔥 \n Action is the foundational key to all success."
                }else{
                    let taskNameReal = taskName[i]
                    let taskTime = timeForToday[i]
                    
                    swipeLb.text = "\(taskNameReal) \n \(taskTime)"
              
                }
                
            }else{
                
                if taskName.count == 0 {
                    swipeLb.text = " 👏🏻 \n Add Some More"
                }else{
                    let taskNameReal = taskName[i]
                    let taskDate = dateForUpcoming[i]
                    swipeLb.text = "\(taskNameReal) \n \(taskDate)"
                }
                
            }
            
            rotation = CGAffineTransform(rotationAngle: 0)
            
            scaledAndRotated = rotation.scaledBy(x: 1, y: 1)
            
            swipeLb.transform = scaledAndRotated
            
            swipeLb.center = CGPoint(x: view.frame.width/2, y: view.frame.height/2)
        }
    }
    
    
    
    func AppendToTodayAndUpcoming() {
        let currentDate = dateFormatter.string(from: date)
        
        let currentDateToCompare = dateFormatter.date(from: currentDate)
        
        
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
    
            request.sortDescriptors = [NSSortDescriptor(key: "time", ascending: true)]
            request.predicate = NSPredicate(format: "done = %@", "true")
            request.returnsObjectsAsFaults = false
            
            if let coreDataToDos = try? context.fetch(request) as? [Task] {
                if let theToDos = coreDataToDos {
                    
                    for data in theToDos {
                       
                        if let dateToCompare = dateFormatter.date(from: data.date!) {
                            if (currentDateToCompare?.isEqualTo(dateToCompare))!{
                                todayList.append(data)
                            }else if (currentDateToCompare?.isSmallerThan(dateToCompare))!{
                                upComingList.append(data)
                            }
                        }
                        
                        
                    }
                    
                    
                }
            }
        }
    }
    
    func fetchTask(nameTask : String, later : Bool,upcoming : Bool) {
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
            request.predicate = NSPredicate(format: "%K = %@", "name", "\(nameTask)")
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coreDataToDos = try? context.fetch(request) as? [Task] {
                if let theToDos = coreDataToDos {
                    
                    for data in theToDos {
                        if later {
                            if upcoming {
                                var dateComponents = DateComponents()
                                dateComponents.day = 1
                                
                                let formatter = DateFormatter()
                                formatter.dateStyle = .medium
                                guard let dateOfTheTask = data.date else {return}
                                guard let dateToAdd = formatter.date(from: dateOfTheTask) else {return}
                              
                                guard let futureDate = Calendar.current.date(byAdding: dateComponents, to: dateToAdd) else {return}
                                data.date = formatter.string(from: futureDate)
                                
                                  try? context.save()
                            }else{
                                let formatter = DateFormatter()
                                formatter.dateFormat = "h:mm a"
                                formatter.amSymbol  = "AM"
                                formatter.pmSymbol = "PM"
                                guard let dataTime = data.time else {return}
                                let time =  formatter.date(from: dataTime)
                                guard let newTime = time?.addingTimeInterval(10 * 60) else {return}
                                
                                data.time = formatter.string(from: newTime)
                                
                                try? context.save()
                            }
                       
                            
                        }else{
                              data.done = true
                            
                              try? context.save()
                        }
                       
                        
                        
                    }
                }
            }
        }
    }
    

}



extension TodayListViewController : TwicketSegmentedControlDelegate {
    func didSelect(_ segmentIndex: Int) {
        self.segmentindex = segmentIndex
        removeTaskName()
        AppendToTodayAndUpcoming()
        appendToTaskName()
        displaySwipeLabel()
    }
    
    
}

extension TodayListViewController : SPStorkControllerDelegate{
    
}
