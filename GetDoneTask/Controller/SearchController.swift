//
//  SearchController.swift
//  GetDoneTask
//
//  Created by Thang Thoi Truong on 5/27/19.
//  Copyright © 2019 Thang Thoi Truong. All rights reserved.
//

import UIKit
import CoreData
import ChameleonFramework
import ExpyTableView
import IGColorPicker
import EmptyDataSet_Swift

/// NSPredicate(format: "message CONTAINS[c] 'fix'")

struct ProjectdataToSearchTaskToPopulate {
    var titleProject : String
    var task : [Task]
}

class SearchController: UIViewController {
    

    @IBOutlet weak var searchBarTextField: UISearchBar!
    
    
    @IBOutlet weak var tableView: ExpyTableView!
    
//    @IBOutlet weak var collectionViewcolor: UICollectionView!
    
    @IBOutlet weak var collectionViewcolor: ColorPickerView!
    
    @IBOutlet weak var colorShowView: UIView!
    
    let cellResuseIdentifier = "cellResuse"
    
    let cellResuseIdentifier2 = "cellResuse2"
    
    let projectResuer = "projectResuseIdentifier"
    
    let collectionReuseIdentifier = "resueForCollec"
    var dataToSearchTask = [Task]()
    
    var dataToSearchProject = [String]()
    
    var projectToPopulate = [ProjectdataToSearchTaskToPopulate]()
    
    var dataToSearchEvent = [Event]()
    
    var dataTask = [Task]()
    
    var collectionViewListColor = [String]()
    
    var arrayForAllThings = [[Any]]()
    
    var hexString = String()
    
    var rowSelectColor = Int()
    
    // For Fetch Project
    var doneTask = [Task]()
    var taskChild = [Task]()
    var projectDataToPopulate : projectData?
    
    
    var imageAnimation: CAAnimation? {
        let animation = CABasicAnimation.init(keyPath: "transform")
        animation.fromValue = NSValue.init(caTransform3D: CATransform3DIdentity)
        animation.toValue = NSValue.init(caTransform3D: CATransform3DMakeRotation(.pi/2, 0.0, 0.0, 1.0))
        animation.duration = 0.25
        animation.isCumulative = true
        animation.repeatCount = MAXFLOAT
        
        return animation;
    }
    
    var image: UIImage? {
        return UIImage.init(named: "clap")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        searchBarTextField.delegate = self
        
//        tableView.delegate = self
//        tableView.dataToSearchTaskSource = self
//
        collectionViewcolor.delegate = self
        collectionViewcolor.layoutDelegate = self

        // Search Ben Kia
//        tableView.expandableDelegate = self
//        tableView.animation = .automatic
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.emptyDataSetDelegate = self
        tableView.emptyDataSetSource = self
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 68
        
        //Alter the animations as you want
        tableView.expandingAnimation = .none
        tableView.collapsingAnimation = .none
        
        
//        tableView.tableFooterView = UIView()
//        tableView.tableHeaderView = nil
        
        
        self.tableView.register(UINib(nibName: "ToDoCell", bundle: nil), forCellReuseIdentifier: toDoCellIdentifier)
        self.tableView.register(UINib(nibName: "EventAndToDoCell", bundle: nil), forCellReuseIdentifier: EventCellindetifier)
        self.tableView.register(UINib(nibName: "ProjectTableViewCell", bundle: nil), forCellReuseIdentifier: projectResuer)
        
        
      
        
    }
    @IBAction func moreColorTapped(_ sender: Any) {
        performSegue(withIdentifier: "goToColor", sender: nil)
        
    }
    
    
    @IBAction func clearTapped(_ sender: Any) {
        hexString = ""
        loadItems()
        colorShowView.backgroundColor = FlatWhite()
    }
    
    func assignToColorPicker(){
        var i = 0
        var collectionViewColorArray = [UIColor]()
        if collectionViewListColor.count < 3{
                print("This is collection view list color\(collectionViewListColor)")
                print("Nho jon 3")
            while i < collectionViewListColor.count{
                guard let color = UIColor(hexString: collectionViewListColor[i]) else {return}
                
                collectionViewColorArray.append(color)
                i += 1
            }
            collectionViewColorArray.append(UIColor.white)
            collectionViewColorArray.append(UIColor.white)
            collectionViewColorArray.append(UIColor.white)
            collectionViewcolor.colors = collectionViewColorArray
            }else{
        
            
                while i < collectionViewListColor.count && i < 5{
                    guard let color = UIColor(hexString: collectionViewListColor[i]) else {return}
                    
                    collectionViewColorArray.append(color)
                    i += 1
                }
                
                collectionViewcolor.colors = collectionViewColorArray
            }
            
        

        if hexString == ""{
            colorShowView.backgroundColor = FlatWhite()
        }else{
            colorShowView.backgroundColor = UIColor(hexString: hexString)
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
      
       collectionViewcolor.selectionStyle = .none
       
        if hexString == ""{
            loadItems()
             assignToColorPicker()
        }else {
            clearOutEverything()
           
            fetchTaskWithColor(color: hexString)
            fetchEventWith(color: hexString)
            fetchProjectWithColor(color: hexString)
            appendToProjectList()
            assignToColorPicker()
            tableView.reloadData()
        }
      print(collectionViewListColor)

        print("HEx Stirng for this \(self.hexString)")
        
    }
    func fetchTask(){
          var collectionColor = [String]()
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
//            request.predicate = NSPredicate(format: "%K = %@", "name", "\(nameTask)")
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coredataToSearchTaskToDos = try? context.fetch(request) as? [Task] {
                if let theToDos = coredataToSearchTaskToDos {
                    
                    var dataTaskToDelete = [Task]()
                    for data in theToDos {
                     
                        dataToSearchTask.append(data)
                        dataTaskToDelete.append(data)
                        arrayForAllThings.append(dataTaskToDelete)
                      
                        if let hexStringColor = data.hexString {
                            if hexStringColor == "#ffffff" {
                                
                            }else{
                                   collectionColor.append(hexStringColor)
                            }
                         
                         
                            
                        }
                        
                        dataTaskToDelete.removeAll()
                    }
                    
                       collectionViewListColor += collectionColor.unique()
                }
            }
        }
        
        print("Collection after task\(collectionViewListColor)")
    }
    func fetchTaskWith(nameTask : String){
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
            /// NSPredicate(format: "message CONTAINS[c] 'fix'")
            request.predicate = NSPredicate(format: "name CONTAINS[c] '\(nameTask)'")
           
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coredataToSearchTaskToDos = try? context.fetch(request) as? [Task] {
                if let theToDos = coredataToSearchTaskToDos {
                    
                     var dataTaskToDelete = [Task]()
                    for data in theToDos {
                        
                        dataToSearchTask.append(data)
                        dataTaskToDelete.append(data)
                        arrayForAllThings.append(dataTaskToDelete)
                   
                        
                        dataTaskToDelete.removeAll()
                    }
                        
                    }
                }
            }
        }
    
    func fetchTaskWithColor(color : String){
      
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
            request.predicate = NSPredicate(format: "%K = %@", "hexString", "\(color)")
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coredataToSearchTaskToDos = try? context.fetch(request) as? [Task] {
                if let theToDos = coredataToSearchTaskToDos {
                    
                    var dataTaskToDelete = [Task]()
                    for data in theToDos {
                        
                        dataToSearchTask.append(data)
                        dataTaskToDelete.append(data)
                        arrayForAllThings.append(dataTaskToDelete)
                        
                  
                        
                        dataTaskToDelete.removeAll()
                    }
                    
                   
                }
            }
        }
    }
    
    func fetchEvent(){
        var collectionColor = [String]()
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Event")
            //            request.predicate = NSPredicate(format: "%K = %@", "name", "\(nameTask)")
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coredataToSearchTaskToDos = try? context.fetch(request) as? [Event] {
                if let theToDos = coredataToSearchTaskToDos {
                    var dataEventToDelete = [Event]()
                    for data in theToDos {
                        
                        dataToSearchEvent.append(data)
                        dataEventToDelete.append(data)
                        arrayForAllThings.append(dataEventToDelete)
                        
                        
                        dataEventToDelete.removeAll()
                        
                        if let hexStringColor = data.hexString {
                            if hexStringColor == "#ffffff" {
                                
                            }else{
                                collectionColor.append(hexStringColor)
                            }
                            
                            
                            
                        }
                        
                    }
                     collectionViewListColor += collectionColor.unique()
                    
                }
                
            }
            }
          print("Collection after event\(collectionViewListColor)")
        }
    
    func fetchEventWith(nameEvent : String){
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Event")
          
            request.predicate = NSPredicate(format: "name CONTAINS[c] '\(nameEvent)'")
            
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coredataToSearchTaskToDos = try? context.fetch(request) as? [Event] {
                if let theToDos = coredataToSearchTaskToDos {
                    var dataEventToDelete = [Event]()
                    for data in theToDos {
                        
                        dataToSearchEvent.append(data)
                        dataEventToDelete.append(data)
                        arrayForAllThings.append(dataEventToDelete)
                        
                        
                        dataEventToDelete.removeAll()
                    }
                }
                
            }
        }
    }
    
    func fetchEventWith(color : String){
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Event")
            
            request.predicate = NSPredicate(format: "%K = %@", "hexString", "\(color)")
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coredataToSearchTaskToDos = try? context.fetch(request) as? [Event] {
                if let theToDos = coredataToSearchTaskToDos {
                    var dataEventToDelete = [Event]()
                    for data in theToDos {
                        
                        dataToSearchEvent.append(data)
                        dataEventToDelete.append(data)
                        arrayForAllThings.append(dataEventToDelete)
                        
                        
                        dataEventToDelete.removeAll()
                    }
                }
                
            }
        }
    }
    
    func fetchProject(){
        var collectionColor = [String]()
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Project")
            //            request.predicate = NSPredicate(format: "%K = %@", "name", "\(nameTask)")
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coredataToSearchTaskToDos = try? context.fetch(request) as? [Project] {
                if let theToDos = coredataToSearchTaskToDos {
                    
                    for data in theToDos {
                        
                        dataToSearchProject.append(data.name!)
                        
                        if let hexStringColor = data.hexString {
                            if hexStringColor == "#ffffff" {
                                
                            }else{
                                collectionColor.append(hexStringColor)
                            }
                            
                        }
                        
                        
                    }
                  
                     collectionViewListColor += collectionColor.unique()
                    collectionViewListColor = collectionViewListColor.unique()
                    
                }
            }
        }
          print("Collection after project\(collectionViewListColor)")
    }
    
    func appendToProjectList(){
        var i = 0
        
        while i < dataToSearchProject.count{
            if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
                let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
              request.predicate = NSPredicate(format: "%K = %@", "project", "\(dataToSearchProject[i])")
                request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
                request.returnsObjectsAsFaults = false
                
                if let coredataToSearchTaskToDos = try? context.fetch(request) as? [Task] {
                    if let theToDos = coredataToSearchTaskToDos {
                        
                        
//                        var projectDataToDelete = [ProjectdataToSearchTaskToPopulate]()
                        var projectDataToDelete = [Any]()
                        
                      
                        
                        projectDataToDelete.append(dataToSearchProject[i])
                        for data in theToDos {
                            
                            dataTask.append(data)
                            
                           
                            projectDataToDelete.append(data)
                        }
                        
                       
                    arrayForAllThings.append(projectDataToDelete)
                       projectDataToDelete.removeAll()
                        
                          projectToPopulate.append(ProjectdataToSearchTaskToPopulate(titleProject: dataToSearchProject[i], task: dataTask))
                        dataTask.removeAll()
                        

                    }
                }
            }
            i += 1
        }
        
    
    }
    
    func fetchProjectWithCondition(nameProject : String){
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Project")
           request.predicate = NSPredicate(format: "name CONTAINS[c] '\(nameProject)'")
            
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coredataToSearchTaskToDos = try? context.fetch(request) as? [Project] {
                if let theToDos = coredataToSearchTaskToDos {
                    
                    for dataToSearchTask in theToDos {
                        
                dataToSearchProject.append(dataToSearchTask.name!)
                        
                    }
                }
            }
        }
    }
    
    func fetchProjectWithName(nameProject : String) -> Project?{
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Project")
           request.predicate = NSPredicate(format: "%K = %@", "name", "\(nameProject)")
            
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coredataToSearchTaskToDos = try? context.fetch(request) as? [Project] {
                if let theToDos = coredataToSearchTaskToDos {
                    
                    for dataToSearchTask in theToDos {
                        
                       return dataToSearchTask
                        
                    }
                }
            }
        }
        return nil
    }
    func fetchProjectWithColor(color : String){
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Project")
         
           request.predicate = NSPredicate(format: "%K = %@", "hexString", "\(color)")
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coredataToSearchTaskToDos = try? context.fetch(request) as? [Project] {
                if let theToDos = coredataToSearchTaskToDos {
                    
                    for dataToSearchTask in theToDos {
                        
                        dataToSearchProject.append(dataToSearchTask.name!)
                        
                    }
                }
            }
        }
    }
   
    func fetchProjectWithNameAndReturnAnProjectObject(nameProject : String) -> Project?{
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Project")
           request.predicate = NSPredicate(format: "%K = %@", "name", "\(nameProject)")
            
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coredataToSearchTaskToDos = try? context.fetch(request) as? [Project] {
                if let theToDos = coredataToSearchTaskToDos {
                    
                    for dataToSearchTask in theToDos {
                        
                        return dataToSearchTask
                        
                    }
                }
            }
        }
        
        return nil
    }
    func fetchFromProject(nameProject : String) -> projectData?{
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
            request.predicate = NSPredicate(format: "%K = %@", "project", "\(nameProject)")
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coreDataToDos = try? context.fetch(request) as? [Task] {
                if let theToDos = coreDataToDos {
                    
                    for data in theToDos {
                        if data.done {
                            doneTask.append(data)
                        }
                        
                        taskChild.append(data)
                        
                    }
                    
                    guard let projectFetchFromName = fetchProjectWithNameAndReturnAnProjectObject(nameProject: nameProject) else {return nil}
                    if let date = projectFetchFromName.date, let hexString = projectFetchFromName.hexString{
                   return   projectData(nameProject: nameProject, date: date, taskChild: taskChild, doneTask: doneTask, hexString: hexString)
                        
                    }
       
                    taskChild.removeAll()
                    doneTask.removeAll()
                    
                }
            }
        }
      return nil
    }

    
}

extension SearchController : ExpyTableViewDelegate,ExpyTableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
   
        if indexPath.section < dataToSearchTask.count{
// Task
              return UITableViewCell()
        }else if indexPath.section >= dataToSearchTask.count && indexPath.section < (dataToSearchTask.count + dataToSearchEvent.count){
   // Event
        }else if indexPath.section >= (dataToSearchTask.count + dataToSearchEvent.count) {
    // Project
            if indexPath.row == 0 {
                
            }else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: toDoCellIdentifier) as? ToDoCell else {return UITableViewCell()}
                guard let dataToPutIn = arrayForAllThings[indexPath.section][indexPath.row] as? Task else {return UITableViewCell()}
                
                if let name = dataToPutIn.name, let time = dataToPutIn.time, let hexString = dataToPutIn.hexString,let projectName = dataToPutIn.project  {
                    
                    cell.configureCell(taskLb: name, doneCheck: dataToPutIn.done, time: time, project: projectName, date: "", hexString: hexString, important: dataToPutIn.important )
                }
                return cell
            }
           
        }
       
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell {
        // The one youwant to seee first in this case the project name and the task name in the task\

        if section < dataToSearchTask.count{
            // Task Cell
      
            if dataToSearchTask.count == 0 {
                return UITableViewCell()
            }else{
      
                guard let cell = tableView.dequeueReusableCell(withIdentifier: toDoCellIdentifier) as? ToDoCell else {return UITableViewCell()}
                let dataToPutIn = dataToSearchTask[section]

                if let name = dataToPutIn.name, let time = dataToPutIn.time, let hexString = dataToPutIn.hexString,let projectName = dataToPutIn.project  {

                    cell.configureCell(taskLb: name, doneCheck: dataToPutIn.done, time: time, project: projectName, date: "", hexString: hexString, important: dataToPutIn.important )
                }
                return cell
            }

        }else if section >= dataToSearchTask.count && section < (dataToSearchTask.count + dataToSearchEvent.count) {
            //Event Cell
           
            if dataToSearchEvent.count == 0{
                return UITableViewCell()
            }else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: EventCellindetifier) as? EventAndToDoCell else {return UITableViewCell()}
              let eventCellToPutint = arrayForAllThings[section] as? [Event]
                guard let eventCellForReal = eventCellToPutint?.first else {return UITableViewCell()}
                
                cell.configureCell(dayLb: (eventCellForReal.date)!, timeLb: (eventCellForReal.time)!, nameEvent: eventCellForReal.name!,hexString: eventCellForReal.hexString!)
                cell.layoutMargins = UIEdgeInsets.zero
           
             return cell
                
             
            }
         
            
        }else if section >= (dataToSearchTask.count + dataToSearchEvent.count){
            // Project Cell
            guard let cell = tableView.dequeueReusableCell(withIdentifier: projectResuer) as? ProjectTableViewCell else {return UITableViewCell()}
//            cell.textLabel?.text = arrayForAllThings[section].first as? String
            guard let nameProject = arrayForAllThings[section].first as? String else {return UITableViewCell()}
            guard let project = fetchProjectWithName(nameProject: nameProject) else {return UITableViewCell()}
            if let projectName = project.name, let date = project.date,let time = project.time, let colorString = project.hexString {
                 cell.cofigureCell(date: date, time: time, projectName: projectName, colorString: colorString)
            }
           
            return cell
        }
        
       return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section < dataToSearchTask.count{
           return 1
        }else if section >= dataToSearchTask.count && section < (dataToSearchTask.count + dataToSearchEvent.count){
             return 1
        }else if section >= (dataToSearchTask.count + dataToSearchEvent.count) {
             return arrayForAllThings[section].count
        }
        return arrayForAllThings[section].count
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
  
        return arrayForAllThings.count
       
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        
        if dataToSearchTask.count == 0{
            if dataToSearchEvent.count == 0{
                if dataToSearchProject.count == 0{
                    return ""
                }else {
                    return "Project"
                }
               
                
            }else{
                if section == 0 {
                    return "Event"
                }else if section == dataToSearchEvent.count {
                    return "Project"
                }
            }
           
        }else if dataToSearchEvent.count == 0 {
            if dataToSearchTask.count == 0{
                if dataToSearchProject.count == 0{
                    return ""
                }else {
                    return "Project"
                }
            }else {
                if section == 0{
                    return "Task"
                }else if section == dataToSearchTask.count {
                    return "Project"
                }
            }
        }else if dataToSearchProject.count == 0{
            if dataToSearchTask.count == 0{
                if dataToSearchEvent.count == 0{
                    return ""
                }else {
                    return "Event"
                }
            }else {
                if section == 0 {
                    return "Task"
                }else if section == dataToSearchTask.count {
                    return "Event"
                }
            }
        }else if dataToSearchProject.count != 0 && dataToSearchEvent.count != 0 && dataToSearchTask.count != 0{
            if section == 0{
                return "Task"
            }else if section == dataToSearchTask.count {
                return "Event"

            }else if section == (dataToSearchTask.count + dataToSearchEvent.count){
                return "Project"
            }
        }
       
      return nil

    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Did Select
        if indexPath.section < dataToSearchTask.count{
            // Task
            
            performSegue(withIdentifier: "goToEdit", sender: dataToSearchTask[indexPath.section])
        }else if indexPath.section >= dataToSearchTask.count && indexPath.section < (dataToSearchTask.count + dataToSearchEvent.count){
           // Event
            if let nameEvent = arrayForAllThings[indexPath.section].first as? Event {
                  performSegue(withIdentifier: goToEditAndEventSegue, sender: nameEvent)
            }
        }else if indexPath.section >= (dataToSearchTask.count + dataToSearchEvent.count) {
           // project
            
            // I have a name I need to fetch from that name
            if let nameProject = arrayForAllThings[indexPath.section].first as? String {
                  let projectToPass = fetchFromProject(nameProject: nameProject)
        
                performSegue(withIdentifier: "goToShowProject", sender: projectToPass)
                
            }
          
          
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToEdit"{
            guard let editVC = segue.destination as? EditTaskVC else {return}
            if let itemTask = sender as? Task{
                  editVC.selectedItem = itemTask
            }
       
        }else if segue.identifier == "goToShowProject"{
              guard let showProject = segue.destination as? ShowProjectVC else {return}
            if let projectForData = sender as? projectData {
                 showProject.projectToPoulateForThisView = projectForData
            }
           
        }else if segue.identifier == "goToColor"{
            guard let colorVC = segue.destination as? tagColorVC else {return}
            colorVC.delegateColor = self
            colorVC.addOrNot = false
        }else if segue.identifier == goToEditAndEventSegue{
            guard let editEventAndProject = segue.destination as? EventAndProjectEditVC else {return}
            editEventAndProject.selectedEvent = sender as? Event
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section >= dataToSearchTask.count && indexPath.section < (dataToSearchTask.count + dataToSearchEvent.count){
            return 162
        }
        return 68
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       
        if section == 0 {
            return 68
        }else if section == dataToSearchTask.count {
            return 68
        }else if section == (dataToSearchTask.count + dataToSearchEvent.count ){
            return 68
        }
        return 0
    }
    
    func tableView(_ tableView: ExpyTableView, expyState state: ExpyState, changeForSection section: Int) {
        
        switch state {
        case .willExpand:
            print("WILL EXPAND")
            
        case .willCollapse:
            print("WILL COLLAPSE")
            
        case .didExpand:
            print("DID EXPAND")
            
        case .didCollapse:
            print("DID COLLAPSE")
        }
    }
    
    func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
        return true

    }
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        print("Hi")
        DispatchQueue.main.async {
            self.searchBarTextField.resignFirstResponder()
        }
    }
}


extension SearchController : MyColorSendindDelegate{
    func sendHexCodeToAddVC(myData: String) {
        self.hexString = myData
    }
    
    func sendColorToAddVC(selectedColor: UIColor) {
        
    }
    
    
}

extension SearchController : ColorPickerViewDelegate,ColorPickerViewDelegateFlowLayout {
    
    func colorPickerView(_ colorPickerView: ColorPickerView, didSelectItemAt indexPath: IndexPath) {
        let color = colorPickerView.colors[indexPath.row]
        hexString = color.hexValue()
        self.rowSelectColor = indexPath.row
        
      assignWithColor()
    }
    
    func assignWithColor(){
        clearOutEverything()
        assignToColorPicker()
        fetchTaskWithColor(color: hexString)
        fetchEventWith(color: hexString)
        fetchProjectWithColor(color: hexString)
        appendToProjectList()
        tableView.reloadData()
    }
    
    func colorPickerView(_ colorPickerView: ColorPickerView, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        // Space between cells
        return 12.0
    }
    
    func colorPickerView(_ colorPickerView: ColorPickerView, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 15.0
    }
    
    
}


extension SearchController : UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        clearOutEverything()
        fetchTaskWith(nameTask: searchBar.text!)
        fetchEventWith(nameEvent: searchBar.text!)
        fetchProjectWithCondition(nameProject: searchBar.text!)
        appendToProjectList()
        print("Array of all things after Search \(arrayForAllThings)")
        tableView.reloadData()
        
    }
    func clearOutEverything(){
        dataToSearchTask.removeAll()
        dataToSearchProject.removeAll()
        dataToSearchEvent.removeAll()
        arrayForAllThings.removeAll()
        projectToPopulate.removeAll()
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text?.count == 0 {
            loadItems()

            DispatchQueue.main.async {
                searchBar.resignFirstResponder()
            }

        }
    }
    
    func loadItems(){
        clearOutEverything()
        fetchTask()
        fetchEvent()
        fetchProject()
       
        appendToProjectList()
        tableView.reloadData()
    }
}

extension Sequence where Iterator.Element: Hashable {
    func unique() -> [Iterator.Element] {
        var seen: [Iterator.Element: Bool] = [:]
        return self.filter { seen.updateValue(true, forKey: $0) == nil }
    }
}






extension SearchController : EmptyDataSetSource,EmptyDataSetDelegate{
    
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
      
        return image
    }
    
    func imageAnimation(forEmptyDataSet scrollView: UIScrollView) -> CAAnimation? {
        return imageAnimation
    }
    
    //    func buttonTitle(forEmptyDataSet scrollView: UIScrollView, for state: UIControl.State) -> NSAttributedString? {
    //        return NSAttributedString(string: "Please add some")
    //    }
    //
    //    func buttonBackgroundImage(forEmptyDataSet scrollView: UIScrollView, for state: UIControl.State) -> UIImage? {
    //        return UIImage(named: "GroupSele")
    //    }
    //
    //
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
    
         return NSAttributedString(string: "There's Nothing To Search")
    }
    
    func backgroundColor(forEmptyDataSet scrollView: UIScrollView) -> UIColor? {
        return UIColor.white
        
    }
    
    func emptyDataSet(_ scrollView: UIScrollView, didTapButton button: UIButton) {
        print("Tapp Buton")
    }
    
    func emptyDataSet(_ scrollView: UIScrollView, didTapView view: UIView) {
        print("Tappp View")
    }
    
    func verticalOffset(forEmptyDataSet scrollView: UIScrollView) -> CGFloat {
        
        return 15
    }
    
    func spaceHeight(forEmptyDataSet scrollView: UIScrollView) -> CGFloat {
        return 10
    }
}
