
//
//  tagColorVC.swift
//  GetDoneTask
//
//  Created by Thang Thoi Truong on 5/27/19.
//  Copyright © 2019 Thang Thoi Truong. All rights reserved.
//
import UIKit
import IGColorPicker
import ChameleonFramework

protocol MyColorSendindDelegate {
    func sendHexCodeToAddVC(myData: String)
    func sendColorToAddVC(selectedColor : UIColor)
    
}

protocol MyTaskColorSendingProtocol {
    func sendTask(myData : Task)
}

protocol MyTaskColorProjectSendingProtocol {
    func sendProject(myData : Project)
}

protocol MyTaskColorEventSendingProtocol {
    func sendEvent(myData : Event)
}


class tagColorVC: UIViewController {

    
    @IBOutlet weak var addButton: RoundButton!
    
    
    @IBOutlet weak var colorPicker: ColorPickerView!
    
    let navBar = SPFakeBarView(style: .stork)
    override var preferredStatusBarStyle: UIStatusBarStyle { return .default }
    

    @IBOutlet weak var colorPickerview:ColorPickerView!
    
    var color = UIColor.black
    
    var hexString = String()
    
    var delegateColor : MyColorSendindDelegate?
    
    var delegateTaskColor : MyTaskColorSendingProtocol?
    
    var delegateEventColor : MyTaskColorEventSendingProtocol?
    
     var delegateProjectColor : MyTaskColorProjectSendingProtocol?
    
    var selectedItem  : Task?
    
    var selectedProject : Project?
    
    var selectedEvent : Event?
    
    var addOrNot = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.modalPresentationCapturesStatusBarAppearance = true
        self.navBar.titleLabel.text = "Add"
        self.navBar.leftButton.setTitle("Cancel", for: .normal)
        self.navBar.leftButton.addTarget(self, action: #selector(self.dismissAction), for: .touchUpInside)
        self.view.addSubview(self.navBar)
        
        colorPickerview.delegate = self
        colorPickerview.layoutDelegate = self
        colorPickerview.colors = [FlatRed(),FlatRedDark(),FlatBlue(),FlatSkyBlue(),FlatSand(),FlatGray(),FlatLime(),FlatLimeDark(),FlatMint(),FlatPlum(),FlatTeal(),FlatMagenta(),FlatNavyBlue(),FlatWatermelon(),FlatPowderBlue(),FlatPurple(),FlatYellow(),UIColor.cyan,FlatPink(),FlatCoffee()]
        
        createEverythingFortheLook(color: color)

        if addOrNot {
            addButton.titleLabel?.text = "Add Color"
        }else{
            addButton.titleLabel?.text = "Choose"
        }
    }
    
    @objc func dismissAction() {
        self.dismiss(animated: true)
    }
    
    
    @IBAction func addButtonTapped(_ sender: Any) {
        if self.delegateTaskColor != nil{
            if let itemSelected = self.selectedItem {
                   delegateTaskColor?.sendTask(myData: itemSelected)
            }

        }
        
        if self.delegateColor != nil{
                delegateColor?.sendHexCodeToAddVC(myData: hexString)
        }
        
        // Event nad RPoject Send color
        if self.delegateEventColor != nil{
            if let itemSelected = self.selectedEvent {
                delegateEventColor?.sendEvent(myData: itemSelected)
            }
          
        }
        
        if self.delegateProjectColor != nil{
            if let itemSelected = self.selectedProject {
                delegateProjectColor?.sendProject(myData: itemSelected)
            }
            
        }
      
        dismissAction()
        navigationController?.popViewController(animated: true)
    }
    
    func createEverythingFortheLook(color : UIColor){
        navBar.titleLabel.textColor = color
        navBar.leftButton.setTitleColor(color, for: .normal)
        addButton.backgroundImageColor = color
    }

}
extension tagColorVC : ColorPickerViewDelegate,ColorPickerViewDelegateFlowLayout {
    func colorPickerView(_ colorPickerView: ColorPickerView, didSelectItemAt indexPath: IndexPath) {
        let color = colorPickerView.colors[indexPath.row]
        hexString = color.hexValue()
        
        if selectedItem != nil{
            selectedItem!.hexString = hexString
        }
        if selectedEvent != nil{
            selectedEvent!.hexString = hexString
        }
        if selectedProject != nil{
            selectedProject!.hexString = hexString
        }
        
        
        print(hexString)
    }
    
    func colorPickerView(_ colorPickerView: ColorPickerView, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        // Space between cells
        return 12.0
    }
    
    func colorPickerView(_ colorPickerView: ColorPickerView, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 15.0
    }
    
    
}
