//
//  EventVC.swift
//  GetDoneTask
//
//  Created by Thang Thoi Truong on 5/27/19.
//  Copyright © 2019 Thang Thoi Truong. All rights reserved.
//

import UIKit
import FSCalendar
import ChameleonFramework
import CoreData
import EmptyDataSet_Swift

let EventCellindetifier = "EventToDoCell"

struct EventPopulate{
    var title : String
    var sectionData : [Event]
}

class EventVC: UIViewController {
 
    
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        //        formatter.dateFormat = "MMMM dd"
        return formatter
    }()
    
    fileprivate lazy var monthFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM yyyy"
        return formatter
    }()
    
    fileprivate lazy var scopeGesture: UIPanGestureRecognizer = {
        [unowned self] in
        let panGesture = UIPanGestureRecognizer(target: self.calendarFS, action: #selector(self.calendarFS.handleScopeGesture(_:)))
        panGesture.delegate = self
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 2
        return panGesture
        }()
    
    
    
    
    fileprivate let gregorian: NSCalendar! = NSCalendar(calendarIdentifier:NSCalendar.Identifier.gregorian)
    
    
    @IBOutlet weak var calendarFS: FSCalendar!
    
    @IBOutlet weak var monthLabel: UILabel!
    
    @IBOutlet weak var nexMonthBtn: UIButton!
    
    @IBOutlet weak var backMonthBtn: UIButton!
    
    @IBOutlet weak var eventTableView: UITableView!
    
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
    
    
    var EventListForCell = [Event]()
    
    var dateToFetch = String()
    
     var dateComponent = DateComponents()
    
    let date = Date()
    
    var eventPopulateData = [EventPopulate]()
    
//    var dateComponent2 = DateComponents()
//    var dayList = [String]()
//    var weekDay = [String]()
//    var dayFullList = [String]()
//
//     let dateFormatter2 = DateFormatter()
//     let dateFormatter3 = DateFormatter()
//
//
    
    var imageAnimation: CAAnimation? {
        let animation = CABasicAnimation.init(keyPath: "transform")
        animation.fromValue = NSValue.init(caTransform3D: CATransform3DIdentity)
        animation.toValue = NSValue.init(caTransform3D: CATransform3DMakeRotation(.pi/2, 0.0, 0.0, 1.0))
        animation.duration = 0.25
        animation.isCumulative = true
        animation.repeatCount = MAXFLOAT
        
        return animation;
    }
    
    var image: UIImage? {
        return UIImage.init(named: "graduate")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        eventTableView.delegate = self
        eventTableView.dataSource = self
        
        eventTableView.emptyDataSetDelegate = self
        eventTableView.emptyDataSetSource = self
        
        calendarFS.dataSource = self
        calendarFS.delegate = self
       
       changeColor(color: FlatBlueDark())
        
        eventTableView.register(UINib(nibName: "EventAndToDoCell", bundle: nil), forCellReuseIdentifier: EventCellindetifier)
        
         monthLabel.text =  self.monthFormatter.string(from: calendarFS.currentPage)
        
        self.view.addGestureRecognizer(self.scopeGesture)
        self.eventTableView.panGestureRecognizer.require(toFail: self.scopeGesture)
        self.calendarFS.scope = .week
        
      
       
       
    }
    override func viewWillAppear(_ animated: Bool) {
        EventListForCell.removeAll()
         fetchFromEventEverything()
        print(EventListForCell)
        eventTableView.reloadData()
    }
    func changeColor(color : UIColor){
        var  i = 0
        nexMonthBtn.setTitleColor(color, for: .normal)
        backMonthBtn.setTitleColor(color, for: .normal)
        monthLabel.textColor = color
        while i < 7 {
            calendarFS.calendarWeekdayView.weekdayLabels[i].textColor = color
            i += 1
        }
        calendarFS.tintColor = color
    }
    

    
    func plusOrMinusMonth(monthToAdd : Int) -> Date{
        let monthsToAdd = monthToAdd
        var dateComponent = DateComponents()
        dateComponent.month = monthsToAdd
        return  Calendar.current.date(byAdding: dateComponent, to: calendarFS.currentPage)!
        
    }
    

    @IBAction func backMonthBtnTapped(_ sender: Any) {
        let backMonthDay = plusOrMinusMonth(monthToAdd: -1)
        calendarFS.setCurrentPage(backMonthDay, animated: true)
    }
    
    @IBAction func nexMonthBtnTapped(_ sender: Any) {
        let nextMonthDay = plusOrMinusMonth(monthToAdd: 1)
        calendarFS.setCurrentPage(nextMonthDay, animated: true)
    }
    
    
    
    func fetchFromEventEverything(){
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Event")
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
               request.sortDescriptors = [NSSortDescriptor(key: "time", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coreDataToDos = try? context.fetch(request) as? [Event] {
                if let theToDos = coreDataToDos {
                    
                    for data in theToDos {
                        
                        EventListForCell.append(data)
                    }
                    
                    
                }
            }
        }
    }
    
    func fetchFromEvent(nameOfDate : [String]){
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Event")
            
            var i = 0
            
            while i < nameOfDate.count{
                request.predicate = NSPredicate(format: "%K = %@", "date", "\(nameOfDate[i])")
                request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
                 request.sortDescriptors = [NSSortDescriptor(key: "time", ascending: true)]
                request.returnsObjectsAsFaults = false
                
                if let coreDataToDos = try? context.fetch(request) as? [Event] {
                    if let theToDos = coreDataToDos {
                        
                        for data in theToDos {
                            
                            EventListForCell.append(data)
                        }
                        
                        
                    }
                }
                i += 1
            }
        }
        
    }
    
    func fetchFromEventAndReturn(nameOfDate : String) -> [Event]{
         var eventArray = [Event]()
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Event")
           
    
    
                request.predicate = NSPredicate(format: "%K = %@", "date", "\(nameOfDate)")
                request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
                request.sortDescriptors = [NSSortDescriptor(key: "time", ascending: true)]
                request.returnsObjectsAsFaults = false
                
                if let coreDataToDos = try? context.fetch(request) as? [Event] {
                    if let theToDos = coreDataToDos {
                        
                        for data in theToDos {
                            eventArray.append(data)
                           
                        }
                        
                        
                        return eventArray
                    }
                }

        }
        return eventArray
    }
    

}



extension EventVC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
            return EventListForCell.count
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
            guard let cell = eventTableView.dequeueReusableCell(withIdentifier: "EventToDoCell", for: indexPath) as? EventAndToDoCell else {return UITableViewCell()}
            
            if let nameToChoose = EventListForCell[indexPath.row].name, let dateToChoose = EventListForCell[indexPath.row].date, let timeToChoose = EventListForCell[indexPath.row].time,let hexString = EventListForCell[indexPath.row].hexString{
                
                
                if timeToChoose == ""{
                    cell.configureCell(dayLb: dateToChoose, timeLb:"Unspecified", nameEvent: nameToChoose,hexString: hexString)
                }else{
                    cell.configureCell(dayLb: dateToChoose, timeLb: timeToChoose, nameEvent: nameToChoose,hexString: hexString)
                }
                
                
            }
            
            
            
            return cell
        
        
    
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 162
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: goToEditAndEventSegue, sender: EventListForCell[indexPath.row])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == goToEditAndEventSegue {
            guard let eventAndProjectVC = segue.destination as? EventAndProjectEditVC else {return}
            eventAndProjectVC.selectedEvent = sender as? Event
            
        }
    }
    func cancelNotification( task : Event){
        if let name = task.name {
            appDelegate?.stopLocalNotification(identifier: name)
        }
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
  
        let deleteAction = removeRow(indexPath: indexPath)
        return deleteAction
    }
    
    func removeRow(indexPath : IndexPath) -> [UITableViewRowAction]{
        
        let taskToDelete = EventListForCell[indexPath.row]
        
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") { (rowAction, indexPath) in
            
            if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
                
                context.delete(taskToDelete)
                
                try? context.save()
                
                self.EventListForCell.remove(at: indexPath.row)
                
                print("EventListFor Cell Count\(self.EventListForCell.count)")
                self.eventTableView.deleteRows(at: [indexPath], with: .fade)
                
                self.cancelNotification(task: taskToDelete)
                
            }
            
            
        }
        
        
        return [deleteAction]
    }

    
    
}

extension EventVC : FSCalendarDelegate,FSCalendarDataSource{
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calendarHeightConstraint.constant = bounds.height
        self.view.layoutIfNeeded()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
    
        let selectedDateFromCalendar = calendar.selectedDates.map({self.dateFormatter.string(from: $0)})
        
        print(selectedDateFromCalendar)
        
        EventListForCell.removeAll()
        
       fetchFromEvent(nameOfDate: selectedDateFromCalendar)
        
       
       
      eventTableView.reloadData()
        
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        let selectedDates = calendar.selectedDates.map({self.dateFormatter.string(from: $0)})
        if selectedDates.count == 0 {
           
            EventListForCell.removeAll()
            fetchFromEventEverything()
            eventTableView.reloadData()
        
        }else{
           EventListForCell.removeAll()
            fetchFromEvent(nameOfDate: selectedDates)
            eventTableView.reloadData()
            
        }
    }
    
    func calendar(_ calendar: FSCalendar, titleFor date: Date) -> String? {
        if self.gregorian.isDateInToday(date) {
            return "★"
        }
        return nil
    }
    
 
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        monthLabel.text =  self.monthFormatter.string(from: calendar.currentPage)
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillSelectionColorFor date: Date) -> UIColor? {
     return FlatRed()
    }
    
    
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        var index = 0
        let dateToPut = dateFormatter.string(from: date)
        
    
//            while index < tableViewData.count {
//                if tableViewData[index].title.contains(dateToPut) {
//                    let day = tableViewData[index].sectionData.count
//                    return day
//                }
//
//                index += 1
//            }
        
        makeEventPopulate(nameOfDate: dateToPut)
        while  index < eventPopulateData.count {
            if eventPopulateData[index].title.contains(dateToPut) {
                let day = eventPopulateData[index].sectionData.count
                return day
            }
            
            index += 1
        }
        
        return 0
    }
    
    func makeEventPopulate(nameOfDate : String){
        var i = 0
        while i < EventListForCell.count{
            let sectionData = fetchFromEventAndReturn(nameOfDate: nameOfDate)
            let eventToPopulate = EventPopulate(title: nameOfDate, sectionData: sectionData)
            
            eventPopulateData.append(eventToPopulate)
            i += 1
        }
    }
    
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
        return [FlatRed(),FlatBlue(),FlatLime()]
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderDefaultColorFor date: Date) -> UIColor? {
       
        return FlatRed()
    }
}



extension EventVC : UIGestureRecognizerDelegate{
    // MARK: - UIGestureRecognizer
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let shouldBegin = self.eventTableView.contentOffset.y <= -self.eventTableView.contentInset.top
        if shouldBegin {
            let velocity = self.scopeGesture.velocity(in: self.view)
            switch self.calendarFS.scope {
            case .month:
                return velocity.y < 0
            case .week:
                return velocity.y > 0
            }
        }
        return shouldBegin
}


}

extension EventVC : EmptyDataSetSource,EmptyDataSetDelegate{
    
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return image
        
    }
    
    func imageAnimation(forEmptyDataSet scrollView: UIScrollView) -> CAAnimation? {
        return imageAnimation
    }
    
    //    func buttonTitle(forEmptyDataSet scrollView: UIScrollView, for state: UIControl.State) -> NSAttributedString? {
    //        return NSAttributedString(string: "Please add some")
    //    }
    //
    //    func buttonBackgroundImage(forEmptyDataSet scrollView: UIScrollView, for state: UIControl.State) -> UIImage? {
    //        return UIImage(named: "GroupSele")
    //    }
    //
    //
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        
        return NSAttributedString(string: "Don’t regret the past, just learn from it.")
    }
    
    func backgroundColor(forEmptyDataSet scrollView: UIScrollView) -> UIColor? {
        return UIColor.white
        
    }
    
    func emptyDataSet(_ scrollView: UIScrollView, didTapButton button: UIButton) {
        print("Tapp Buton")
    }
    
    func emptyDataSet(_ scrollView: UIScrollView, didTapView view: UIView) {
        print("Tappp View")
    }
    
    func verticalOffset(forEmptyDataSet scrollView: UIScrollView) -> CGFloat {
        
        return 15
    }
    
    func spaceHeight(forEmptyDataSet scrollView: UIScrollView) -> CGFloat {
        return 10
    }
}
