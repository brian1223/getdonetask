//
//  AddVC.swift
//  GetDoneTask
//
//  Created by Thang Thoi Truong on 5/27/19.
//  Copyright © 2019 Thang Thoi Truong. All rights reserved.
//

import UIKit
import DateTimePicker
import ChameleonFramework
import IGColorPicker
import SPStorkController
import SCLAlertView
import CoreData
import NotificationBannerSwift


class AddVC: UIViewController, UITextFieldDelegate{


    @IBOutlet weak var dateLb: UILabel!
    
    @IBOutlet weak var dateNameLb: UILabel!
    
    @IBOutlet weak var timeNameLb: UILabel!
    
    @IBOutlet weak var tagNameLb: UILabel!
    
    @IBOutlet weak var importantNameLb: UILabel!
    @IBOutlet weak var chooseDateBtn: UIButton!
    

    @IBOutlet weak var chooseTimeBtn: UIButton!
    
    
    @IBOutlet weak var timeLb: UILabel!
    
    @IBOutlet weak var nameProjectTextField: UITextField!
    
    @IBOutlet weak var priorityBtn: UIButton!
    
    @IBOutlet weak var colorPickerView: ColorPickerView!
    
    @IBOutlet weak var moreButton: UIButton!
    
    @IBOutlet weak var clearButton: UIButton!
    
    
    @IBOutlet weak var projectNameLb: UILabel!
    
    @IBOutlet weak var projectPickerView: UIPickerView!
    @IBOutlet weak var addButton: RoundButton!
    
    @IBOutlet weak var colorShowLb: UILabel!
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter
    }()
    
       let date = Date()
    
      var appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    
   var dateSelected  = ""
    
    var timeSelected = ""
    
    var hexString = ""
    
    var important = false
    
    var showSwitch = 0
    
    var picker = DateTimePicker()
    
    var color = UIColor()
    
    var selectedColor : UIColor?
   
    
   let navBar = SPFakeBarView(style: .stork)
    override var preferredStatusBarStyle: UIStatusBarStyle { return .default }
    
    
    var placeHolder = NSMutableAttributedString()
    
    var pickerViewDataArray = [String]()
    
    var projectSelected = ""
    
    var colorToSelectAtPicker : UIColor?
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.navigationController?.hidesNavigationBarHairline = true
        
// Color Picker
        
        colorPickerView.delegate = self
        colorPickerView.layoutDelegate = self
        
  
      

        
// important button
        importantButtonState()

// Name Project Text Field


        nameProjectTextField.becomeFirstResponder()
        nameProjectTextField.delegate = self

        
  // Nav Bar
        self.modalPresentationCapturesStatusBarAppearance = true

        self.navBar.leftButton.setTitle("Cancel", for: .normal)

        self.navBar.leftButton.addTarget(self, action: #selector(self.dismissAction), for: .touchUpInside)


        self.view.addSubview(self.navBar)

        switch showSwitch {
        case 0:
            // This is event
       createEverythingForTheLook(color: FlatBlueDark(), textTitle: "Add Event", placeHolderText: "I have a...")
       projectPickerView.isHidden = true
       projectNameLb.isHidden = true
            
        case 1:
            //This is project
            createEverythingForTheLook(color: FlatGreenDark(), textTitle: "Add Project", placeHolderText: "My project is ...")
            
            projectPickerView.isHidden = true
            projectNameLb.isHidden = true
        case 2:
            // This is task
            createEverythingForTheLook(color: FlatRedDark(), textTitle: "Add Task", placeHolderText: "I want to...")
            
    
        default:
            self.navBar.titleLabel.text = "Add Task"
        }
        
        
        projectPickerView.delegate = self
        projectPickerView.dataSource = self
        
    
        
        // down gesture
        let down = UISwipeGestureRecognizer(target : self, action : #selector(self.downSwipe))
        down.direction = .down
        self.view.addGestureRecognizer(down)
        // left
        let right = UISwipeGestureRecognizer(target : self, action : #selector(self.downSwipe))
        right.direction = .right
        self.view.addGestureRecognizer(right)
    }
    
    @objc func downSwipe(){
        dismissAction()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        pickerViewDataArray.removeAll()
        fetchProjectName()
        projectPickerView.reloadAllComponents()
        
        dateLb.text = dateSelected
        timeLb.text = timeSelected
        
       
        
        colorPickerView.colors = [FlatYellow(),FlatBlue(),FlatRed(),FlatMint()]
        
       colorPickerView.selectionStyle = .check
     
    
       showLbColor()
       

        
    }
    
    func showLbColor(){
        if hexString == "" {
            colorShowLb.backgroundColor = FlatWhite()
        }else{
            let colorToShow = UIColor(hexString: hexString)
            colorShowLb.backgroundColor = colorToShow
        }
        
    }
    
    
    func fetchProjectName() {
         pickerViewDataArray.append("none")
        
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            
            if let coreDataToDos = try? context.fetch(Project.fetchRequest()) as? [Project] {
                if let theToDos = coreDataToDos {
                    
                    for data in theToDos {
                     
                        if let projectNameToAppend = data.name {
                            pickerViewDataArray.append(projectNameToAppend)
                            
                        }
                    }
                    
                   
                }
                
                
            }
        }
    }
    
    func createEverythingForTheLook(color : UIColor, textTitle : String,placeHolderText : String){
        self.navBar.titleLabel.text = textTitle
        self.navBar.backgroundColor = color
        self.navBar.titleLabel.textColor = color
        
        self.navBar.leftButton.setTitleColor(color, for: .normal)
        
        
        placeHolder = NSMutableAttributedString(string:placeHolderText, attributes: [NSAttributedString.Key.font:UIFont(name: "Helvetica", size: 35.0)!])
        
        nameProjectTextField.attributedPlaceholder = NSAttributedString(string: placeHolderText,attributes: [NSAttributedString.Key.foregroundColor: color])
        nameProjectTextField.useUnderline(color: color)
        nameProjectTextField.textColor = color
        
        moreButton.setTitleColor(color, for: .normal)
        
        addButton.backgroundImageColor = color
        
        tagNameLb.textColor = color
        
    
       
        importantNameLb.textColor = color
        
        dateLb.textColor =  color
        timeLb.textColor  = color
       dateNameLb.textColor = color
        timeNameLb.textColor = color
        chooseDateBtn.setTitleColor(color, for: .normal)
        priorityBtn.setTitleColor(color, for: .normal)
        
        clearButton.setTitleColor(color, for: .normal)
        projectNameLb.textColor = color
        
        
    }
    
    @objc func dismissAction() {
        self.dismiss(animated: true)
    }
    
    
    func tapToDimissDateAndTime() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = true
        view.addGestureRecognizer(tap)
    }

        // TExt field keyboard should return
    
    @IBAction func dateButtonPressed(_ sender: Any) {
         self.view.endEditing(true)
        performSegue(withIdentifier: "goToChooseDate", sender: self)

        
        
    }
    @IBAction func timeButtonPressed(_ sender: Any) {
          self.view.endEditing(true)
        createCalendar(isDatePicker: false)
    }
    
    
    
    
    // prepare for segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToChooseDate"{
            guard let chooseDateVC = segue.destination as? ChooseDateAndTimeVC else{return}
            
            chooseDateVC.delegate = self
            
            switch showSwitch {
            case 0 :
                return chooseDateVC.color = FlatBlue()
            case 1 :
                return chooseDateVC.color = FlatGreenDark()
            case 2 :
                return chooseDateVC.color = FlatRed()
                
            default:
                return chooseDateVC.color = FlatRed()
            }
        }
        
        if segue.identifier == "toTagColorVC" {
              guard let tagVC = segue.destination as? tagColorVC else{return}
              tagVC.color = self.color
            tagVC.delegateColor = self
        }
 
        
    }
    
    @IBAction func moreTapped(_ sender: Any) {
      performSegue(withIdentifier: "toTagColorVC", sender: self)
    }
    
    @IBAction func clearColorTapped(_ sender: Any) {
        
        colorShowLb.backgroundColor = FlatWhite()
        hexString = UIColor.white.hexValue()
        
    }
    
    
    

    // TextField
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
      
        return true
    }
    
    func createCalendar(isDatePicker : Bool){
        let min = Date().addingTimeInterval(-60 * 60 * 24 * 4)
        let max = Date().addingTimeInterval(480 * 60 * 24 * 4)
         picker = DateTimePicker.create(minimumDate: min, maximumDate: max)
    
        //                picker.dateFormat = "hh:mm aa dd/MM/YYYY"
        if isDatePicker {
              picker.dateFormat = "dd/MM/YYYY"
            picker.isDatePickerOnly = true
        }else{
            picker.dateFormat = "h:mm a"
            picker.is12HourFormat = true
            picker.todayButtonTitle = ""
            picker.isTimePickerOnly = true
        }
      
        
        picker.includeMonth = true // if true the month shows at bottom of date cell
        picker.highlightColor = color
        picker.darkColor = UIColor.darkGray
        picker.doneButtonTitle = "Save time"
        picker.doneBackgroundColor = color
        
        if isDatePicker {
            picker.completionHandler = { date in
                
                let formatter = DateFormatter()
                formatter.dateFormat = "dd/MM/YYYY"
                self.dateLb.text = formatter.string(from: date)
                
            }
        }else{
            picker.completionHandler = { date in
                
                let formatter = DateFormatter()
                formatter.dateFormat = "h:mm a"
                formatter.amSymbol  = "AM"
                formatter.pmSymbol = "PM"
                
                self.timeSelected = formatter.string(from: date)
                self.timeLb.text = formatter.string(from: date)
                
            }
        }
        
//         add tap gesture to dismiss
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissDateAndTime))
        tap.cancelsTouchesInView = true
        tap.numberOfTapsRequired = 2
        self.picker.addGestureRecognizer(tap)

        picker.delegate = self
        picker.show()
    }
    
    
    @objc func dismissDateAndTime(){
        picker.dismissView()
    }
    
    @IBAction func priorityBtnTapped(_ sender: Any) {
        if important {
            important = false
            importantButtonState()
        }else{
            important = true
            importantButtonState()
        }
    }
    
    
    func importantButtonState(){
        if important == false {
            priorityBtn.setTitle("✩", for: .normal)
            
        }else{
            priorityBtn.setTitle("★", for: .normal)
        }
        
    }
    
    func createAlert(title : String,message : String,actionTitle : String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: actionTitle, style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: false, completion: nil)
    }
    
    

    func saveDataTask(entityName : String,project : Bool){
        
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            
            
            let entity = NSEntityDescription.entity(forEntityName: entityName, in: context)
        
            let toDo = NSManagedObject(entity: entity!, insertInto: context)
        
            
            if let titleText = nameProjectTextField.text {
             toDo.setValue(titleText, forKey: "name")
             toDo.setValue(dateSelected, forKey: "date")
             toDo.setValue(timeSelected, forKey: "time")
             toDo.setValue(important, forKey: "important")
             toDo.setValue(false, forKey: "done")
                if self.hexString == "" {
                    if project {
                          toDo.setValue("#ffffff", forKey: "hexString")
                    }else{
                         toDo.setValue("#ffffff", forKey: "hexString")
                    }
                   
                }else{
                     toDo.setValue(hexString, forKey: "hexString")
                }
                if entityName == "Task"{
                    toDo.setValue(projectSelected, forKey: "project")
                }
             
                try? context.save()
                bannerSave()
       
                print("Save successfully into \(entityName)")
                
            }else{
             
            }
        }
        
    }
    
    func bannerSave(){
        
        let banner = NotificationBanner(title: "Great Job",
                                        subtitle: "Save successfully",
                                        style: .success,
                                        colors: FlatGreen() as? BannerColorsProtocol)
        banner.show(queuePosition: .front, bannerPosition: .top, queue: .default, on: self)

    }
    
    
    @IBAction func addButtonTapped(_ sender: Any) {
        if nameProjectTextField.text != "" {
            
            switch showSwitch {
             case 0 :
                
                if dateSelected == ""{
                      SCLAlertView().showError("Please provide enough info", subTitle: "Please check again if you provided the date for your event")
                }else{
                     saveDataTask(entityName: "Event", project: false)
                    changeDateAndTimeToDateObject()
                    addAlready(today: false)
                }
             
             case 1 :
                saveDataTask(entityName: "Project", project: true)
                addAlready(today: false)
                
             case 2 :
                saveDataTask(entityName: "Task", project: false)
                // Put this function before add already
                changeDateAndTimeToDateObject()
                addAlready(today: false)
                
            default:
                 saveDataTask(entityName: "Task", project: false)
                 addAlready(today: false)
            
            }
            
      
            
         
        }else{
          SCLAlertView().showError("Please provide enough info", subTitle: "Please check again if you provided the name for your project")
        }

    }
    
    
    func changeDateAndTimeToDateObject(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd h:mm a"
        
         let formatter2 = DateFormatter()
        formatter2.dateStyle = .medium
        let dateToTransform = formatter2.date(from: dateSelected)
   
        
        let formatter3 = DateFormatter()
        formatter3.dateFormat = "yyyy/MM/dd"
        guard let StringOfDateToTransform2  = dateToTransform else {return}
        let dateActuallyTransform = formatter3.string(from: StringOfDateToTransform2)
        
        
        let dateSecheduleString = "\(dateActuallyTransform) \(timeSelected)"
        
        
        let dateSechedule = formatter.date(from: dateSecheduleString)
        
        if showSwitch == 2 {
            if let name = nameProjectTextField.text,let dateReal = dateSechedule {
                 self.appDelegate?.scheduleNotification(title: "It is time for \(name)", body: "Let get to work", date: dateReal, identifier: name, triggerStyle: "dateSpecific")
                
                print("Schedule notification successful at \(dateSechedule)")
            }
            
          
            
        }else{
            
        }

       
    }

    func addAlready(today : Bool){
        nameProjectTextField.text = ""
     
        if today {
              dateSelected = dateFormatter.string(from: date)
        }else{
            dateSelected = ""
        }
      
        timeSelected = ""
        hexString = ""
        important = false
        projectSelected = ""
        
        dateLb.text = ""
        timeLb.text = ""
        projectPickerView.selectRow(0, inComponent: 0, animated: true)
        colorShowLb.backgroundColor = FlatWhite()
        nameProjectTextField.becomeFirstResponder()
        
       priorityBtn.setTitle("✩", for: .normal)
        
        
//        dismiss(animated: true) {
//            guard let toDoList = self.storyboard?.instantiateViewController(withIdentifier: "toDoListVC") as? toDoListVC else {return}
//            toDoList.reloadRemoveAppend()
//        }
        
    }
    
    
    func fetchTaskName(nameTask : String) -> Task?{
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
            request.predicate = NSPredicate(format: "%K = %@", "name", "\(nameTask)")
            
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coredataToSearchTaskToDos = try? context.fetch(request) as? [Task] {
                if let theToDos = coredataToSearchTaskToDos {
                    
                    for dataToSearchTask in theToDos {
                        
                        return dataToSearchTask
                        
                    }
                }
            }
        }
        
        return nil
    }
    
    func fetchProjectName(nameProject : String) -> Project?{
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Project")
            request.predicate = NSPredicate(format: "%K = %@", "name", "\(nameProject)")
            
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coredataToSearchTaskToDos = try? context.fetch(request) as? [Project] {
                if let theToDos = coredataToSearchTaskToDos {
                    
                    for dataToSearchTask in theToDos {
                        
                        return dataToSearchTask
                        
                    }
                }
            }
        }
        
        return nil
    }

    func fetchEventName(nameEvent : String) -> Event?{
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Event")
            request.predicate = NSPredicate(format: "%K = %@", "name", "\(nameEvent)")
            
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coredataToSearchTaskToDos = try? context.fetch(request) as? [Event] {
                if let theToDos = coredataToSearchTaskToDos {
                    
                    for dataToSearchTask in theToDos {
                        
                        return dataToSearchTask
                        
                    }
                }
            }
        }
        
        return nil
    }
    
}


extension AddVC : UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerViewDataArray.count
    }
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//
//
//        return pickerViewDataArray[row]
//    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let attributedString = NSAttributedString(string: pickerViewDataArray[row], attributes: [NSAttributedString.Key.foregroundColor : color])
        return attributedString
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row == 0 {
            projectSelected = ""
        }else{
             projectSelected = pickerViewDataArray[row]
        }
       
    }
}



extension AddVC : DateTimePickerDelegate {
    func dateTimePicker(_ picker: DateTimePicker, didSelectDate: Date) {

    }
    
    
}

extension AddVC : ColorPickerViewDelegate,ColorPickerViewDelegateFlowLayout {
    func colorPickerView(_ colorPickerView: ColorPickerView, didSelectItemAt indexPath: IndexPath) {
        let color = colorPickerView.colors[indexPath.row]
       hexString = color.hexValue()
    
      showLbColor()
   
      
    }
    
    func colorPickerView(_ colorPickerView: ColorPickerView, didDeselectItemAt indexPath: IndexPath) {
       colorShowLb.backgroundColor = FlatWhite()
    }
   
    
    func colorPickerView(_ colorPickerView: ColorPickerView, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        // Space between cells
        return 15
    }


    
    
}



extension AddVC : MyDataSendingDelegateProtocol{
    func sendDateToAddVC(myData: String) {
        dateSelected = myData
    }
    
    func sendTimeToAddVC(myData: String) {
         timeSelected = myData
    }
    
    
}

extension AddVC : MyColorSendindDelegate{
    func sendHexCodeToAddVC(myData: String) {
        self.hexString = myData
    }
    func sendColorToAddVC(selectedColor: UIColor) {
        self.selectedColor = selectedColor
    }
    
}



//
//        view.addSubview(button)
//        button.addTarget(self, action: #selector(drag(control:event:)), for: UIControl.Event.touchDragInside)
//        button.addTarget(self, action: #selector(drag(control:event:)), for: UIControl.Event.touchDragExit)
//
//
//    }
//
//    @objc func drag(control: UIControl, event: UIEvent) {
//        if let center = event.allTouches!.first?.location(in: self.view) {
//            control.center = center
//        }
//    }
