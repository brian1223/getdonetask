//
//  ChooseDateAndTimeVC.swift
//  GetDoneTask
//
//  Created by Thang Thoi Truong on 5/27/19.
//  Copyright © 2019 Thang Thoi Truong. All rights reserved.
//

import UIKit
import FSCalendar
import DateTimePicker
import ChameleonFramework




protocol MyDataSendingDelegateProtocol {
    func sendDateToAddVC(myData: String)
    func sendTimeToAddVC(myData: String)
}

protocol MySelectedItemDelegateProtocol {
    func sendItem(myData : Task)
}

protocol MySelectedProjectDelegateProtocol {
    func sendItem(myData : Project)
}

protocol MySelectedEventDelegateProtocol {
    func sendItem(myData : Event)
}

class ChooseDateAndTimeVC: UIViewController {
    
    
    let cellIdentifier = "dateAndTimeCell"
    
    @IBOutlet weak var tableView: UITableView!
    
    
    @IBOutlet weak var calendarViewFS: FSCalendar!
    
    @IBOutlet weak var monthLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var nextButtonBtn: UIButton!
    
    @IBOutlet weak var addTimeButton: RoundButton!
    
    @IBOutlet weak var SaveDateButn: RoundButton!
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
    
    
    
    var dateSelected = ""
    
    var timeSelected = ""
    
    var dateAndTimeSelected = ""
    
    var colorSwitch = 0
    
    var picker = DateTimePicker()
    
    var color = UIColor()
    
    var delegate: MyDataSendingDelegateProtocol? = nil
    
    var delegateSelected : MySelectedItemDelegateProtocol? = nil
    
    var delegateEvent : MySelectedEventDelegateProtocol? = nil
    
    var delegateProject : MySelectedProjectDelegateProtocol? = nil
    
    var selectedTask = Task()
    
    var selectedProject : Project?
    
    var selectedEvent : Event?
    
    
    let navBar = SPFakeBarView(style: .stork)
    override var preferredStatusBarStyle: UIStatusBarStyle { return .default }
    
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
//        formatter.dateFormat = "MMMM dd"
        return formatter
    }()
    
    fileprivate lazy var monthFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM yyyy"
        return formatter
    }()
    
    
    fileprivate let gregorian: NSCalendar! = NSCalendar(calendarIdentifier:NSCalendar.Identifier.gregorian)
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.modalPresentationCapturesStatusBarAppearance = true
        self.navBar.titleLabel.text = "Add Date And Time"
        self.navBar.leftButton.setTitle("Cancel", for: .normal)
        self.navBar.leftButton.addTarget(self, action: #selector(self.dismissAction), for: .touchUpInside)
        self.view.addSubview(self.navBar)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        calendarViewFS.delegate = self
        calendarViewFS.dataSource = self
        
        colorForEverything(color: color)
        
        let down = UISwipeGestureRecognizer(target : self, action : #selector(self.dismissAction))
        down.direction = .down
        self.view.addGestureRecognizer(down)
        
  
        
    
    }
    
    @objc func dismissAction() {
        self.dismiss(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         monthLabel.text =  self.monthFormatter.string(from: calendarViewFS.currentPage)
    }
    
    @IBAction func nexMonthTapped(_ sender: Any) {
        let nextMonthDay = plusOrMinusMonth(monthToAdd: 1)
        calendarViewFS.setCurrentPage(nextMonthDay, animated: true)
    }
    
    @IBAction func backMonthTapped(_ sender: Any) {
        let backMonthDay = plusOrMinusMonth(monthToAdd: -1)
         calendarViewFS.setCurrentPage(backMonthDay, animated: true)
    }
    
    
    func plusOrMinusMonth(monthToAdd : Int) -> Date{
        let monthsToAdd = monthToAdd
        var dateComponent = DateComponents()
        dateComponent.month = monthsToAdd
        return  Calendar.current.date(byAdding: dateComponent, to: calendarViewFS.currentPage)!
        
    }
    
    func plusOrMinusDay(dayToAdd : Int) -> Date {
        let dayToAdd = dayToAdd
        var dateComponent = DateComponents()
        dateComponent.day = dayToAdd
        return Calendar.current.date(byAdding: dateComponent, to: Date())!
    }
    
    
    @IBAction func addTimeTapped(_ sender: Any) {
        createTimePicker(color: color)
        
        
    }
    
    @IBAction func saveDateTapped(_ sender: Any) {
        // Task Edit
        
        if self.delegate != nil{
            delegate?.sendDateToAddVC(myData: dateSelected)
              delegate?.sendTimeToAddVC(myData: timeSelected)
        }
        
        if self.delegateSelected != nil{
            selectedTask.date = dateSelected
            selectedTask.time = timeSelected
            
           self.delegateSelected?.sendItem(myData: selectedTask)
        }
        
        // Event Edit and Project Edit
        if let selectedProject2 = self.selectedProject {
            selectedProject2.date = dateSelected
            selectedProject2.time = timeSelected
             self.delegateProject?.sendItem(myData: selectedProject2)
        }
        
        if let selectedEvent3 = self.selectedEvent {
            selectedEvent3.date = dateSelected
            selectedEvent3.time = timeSelected
            self.delegateEvent?.sendItem(myData: selectedEvent3)
        }
        
    
       
        dismiss(animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
//        navigationController?.pushViewController(addVC, animated: true)
    }
    
    
    func createTimePicker(color : UIColor){
        let min = Date()
        //            .addingTimeInterval(-60 * 60 * 24 * 4)
        let max = Date().addingTimeInterval(480 * 60 * 24 * 4)
        picker = DateTimePicker.create(minimumDate: min, maximumDate: max)
        
        //                picker.dateFormat = "hh:mm aa dd/MM/YYYY"
    
            picker.dateFormat = "h:mm a"
            picker.is12HourFormat = true
            picker.todayButtonTitle = ""
            picker.isTimePickerOnly = true
    
        
        
        picker.includeMonth = true // if true the month shows at bottom of date cell
        picker.highlightColor = color
        picker.darkColor = UIColor.darkGray
        picker.doneButtonTitle = "Save Time"
        picker.doneBackgroundColor = color
    
            picker.completionHandler = { date in
                
                let formatter = DateFormatter()
                formatter.dateFormat = "h:mm a"
                formatter.amSymbol  = "AM"
                formatter.pmSymbol = "PM"
                
                let time = formatter.string(from: date)
                self.timeSelected = time
                self.timeLabel.text = time
        }
        
        //         add tap gesture to dismiss
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissDateAndTime))
        tap.cancelsTouchesInView = true
        tap.numberOfTapsRequired = 2
        self.picker.addGestureRecognizer(tap)
        
        picker.delegate = self
        picker.show()
    }
    
    
    @objc func dismissDateAndTime(){
        picker.dismissView()
    }
    
    
    func colorForEverything(color : UIColor){
        // Nav Bar
        var i = 0
        navBar.titleLabel.textColor = color
       self.navBar.leftButton.setTitleColor(color, for: .normal)
        
        // Date, time and month and button Label
        dateLabel.textColor = color
        timeLabel.textColor = color
        monthLabel.textColor = color
        backButton.setTitleColor(color, for: .normal)
        nextButtonBtn.setTitleColor(color, for: .normal)
        while i < 7 {
            calendarViewFS.calendarWeekdayView.weekdayLabels[i].textColor = color
            i += 1
        }
        calendarViewFS.tintColor = color
        addTimeButton.backgroundImageColor = color
        SaveDateButn.backgroundImageColor = color
    }
    
    
}


extension ChooseDateAndTimeVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
     let cell = createCellLook(color: color, indexPath: indexPath)
        return cell
       
    }
    
    func createCellLook(color : UIColor,indexPath : IndexPath) -> UITableViewCell{
         let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        if indexPath.row == 0{
            cell.textLabel?.text = "Today"
            cell.textLabel?.textColor = color
        }else if indexPath.row == 1 {
            cell.textLabel?.text = "Tommorow"
             cell.textLabel?.textColor = color
        }else  if indexPath.row == 2{
            cell.textLabel?.text = "Next Monday"
             cell.textLabel?.textColor = color
        }else  if indexPath.row == 3{
            cell.textLabel?.text = "Any Time"
            cell.textLabel?.textColor = color
        }else  if indexPath.row == 4{
            cell.textLabel?.text = "Any Date"
            cell.textLabel?.textColor = color
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         tableView.deselectRow(at:  tableView.indexPathForSelectedRow!, animated: true)
        
        if let selectedDate = calendarViewFS.selectedDate {
            calendarViewFS.deselect(selectedDate)
        }
        
        createActionForSelectRow(indexPath: indexPath)
    }
    
    func createActionForSelectRow(indexPath : IndexPath){
        let date = Date()
        let tommorow =  plusOrMinusDay(dayToAdd: 1)
        
        if indexPath.row == 0{
            dateSelected = dateFormatter.string(from: date)
            calendarViewFS.setCurrentPage(date, animated: true)
            dateLabel.text = dateSelected
        }else if indexPath.row == 1 {
            dateSelected = dateFormatter.string(from: tommorow)
            calendarViewFS.setCurrentPage(tommorow, animated: true)
            dateLabel.text = dateSelected
            
        }else  if indexPath.row == 2{
            dateSelected = dateFormatter.string(from: Date.today().next(.monday)  )
            calendarViewFS.setCurrentPage(Date.today().next(.monday) , animated: true)
            dateLabel.text = dateSelected
            
        }else  if indexPath.row == 3{
            timeSelected = ""
            timeLabel.text = "Any time"
            
        }else  if indexPath.row == 4{
            dateSelected = ""
            dateLabel.text = "Any date"
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "backToAddVC" {
//            guard let addVC = segue.destination as? AddVC else {return}
//            addVC.dateSelected = self.dateSelected
//            addVC.timeSelected = self.timeSelected
//
//        }
    }
    
}


extension ChooseDateAndTimeVC : FSCalendarDelegate, FSCalendarDataSource,FSCalendarDelegateAppearance{
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calendarHeightConstraint.constant = bounds.height
        self.view.layoutIfNeeded()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
         let selectedDateFromCalendar = calendar.selectedDates.map({self.dateFormatter.string(from: $0)})
       dateLabel.text = selectedDateFromCalendar[0]
        dateSelected = selectedDateFromCalendar[0]
        
        if let indexPathSelected = tableView.indexPathForSelectedRow {
             tableView.deselectRow(at: indexPathSelected, animated: true)
        }
       
    }
    
    func calendar(_ calendar: FSCalendar, titleFor date: Date) -> String? {
        if self.gregorian.isDateInToday(date) {
            return "★"
        }
        return nil
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
     monthLabel.text =  self.monthFormatter.string(from: calendar.currentPage)
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillSelectionColorFor date: Date) -> UIColor? {
        return color
    }
    
    
}


extension ChooseDateAndTimeVC : DateTimePickerDelegate{
    func dateTimePicker(_ picker: DateTimePicker, didSelectDate: Date) {
        
    }
    
    
}

