//
//  toDoListVC.swift
//  GetDoneTask
//
//  Created by Thang Thoi Truong on 5/27/19.
//  Copyright © 2019 Thang Thoi Truong. All rights reserved.
//

import UIKit
import CircleMenu
import SPStorkController
import ChameleonFramework
import CoreData
import EmptyDataSet_Swift
import SCLAlertView

var appDelegate = UIApplication.shared.delegate as? AppDelegate



protocol YourCellDelegate : class {
    func didPressButton(_ tableView:UITableView )
}

struct projectData {
    var nameProject : String
    var date : String
    var taskChild : [Task]
    var doneTask : [Task]
    var hexString : String
}

 let toDoCellIdentifier = "ToDoCell"

class toDoListVC: UIViewController{


    
    @IBOutlet weak var todayTableView: UITableView!
    
    
    @IBOutlet weak var projectCollectionView: UICollectionView!
    
    
    @IBOutlet weak var upcomingTableView: UITableView!
    
    @IBOutlet weak var inboxTableView: UITableView!
    
    
    @IBOutlet weak var todayNumberOfTask: UILabel!
    
    @IBOutlet weak var projectNumberOfTask: UILabel!
    
    @IBOutlet weak var InBoxNumberOfTask: UILabel!
    
    @IBOutlet weak var upcomingNumberOfTask: UILabel!
    
   
    
    let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexString: "A569F5")]

    
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter
    }()
    
    fileprivate lazy var dateFormatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd"
        return formatter
    }()
    
    
    @IBOutlet weak var todayBtn: UIButton!
    
    @IBOutlet weak var projectBtn: UIButton!
    
    @IBOutlet weak var upcomingBtn: UIButton!
    
    @IBOutlet weak var inBoxBtn: UIButton!
    
    
    var todayList = [Task]()
    
    var upComingList = [Task]()
    
    var inBoxList = [Task]()
    
    var projectList = [Project]()
    
    var projectToPopulate = [projectData]()
    
    let date = Date()
    
    var taskChild = [Task]()
    
    var doneTask = [Task]()
    
    var hexString = String()
    
    
    var customView = UIView()
    
    var seletedItemInTableView =  Task()
    
    
    
    
    var imageAnimation: CAAnimation? {
        let animation = CABasicAnimation.init(keyPath: "transform")
        animation.fromValue = NSValue.init(caTransform3D: CATransform3DIdentity)
        animation.toValue = NSValue.init(caTransform3D: CATransform3DMakeRotation(.pi/2, 0.0, 0.0, 1.0))
        animation.duration = 0.25
        animation.isCumulative = true
        animation.repeatCount = MAXFLOAT
        
        return animation;
    }
    
    var image: UIImage? {
        return UIImage.init(named: "party")
    }
    
    var image2: UIImage? {
        return UIImage.init(named: "champ")
    }
    
    var image3: UIImage? {
        return UIImage.init(named: "champp")
    }

    var image4: UIImage? {
        return UIImage.init(named: "medal")
    }
    let items: [(icon: String, color: UIColor)] = [
        ("event", UIColor(red: 0.19, green: 0.57, blue: 1, alpha: 1)),
        ("chart", UIColor(red: 0.22, green: 0.74, blue: 0, alpha: 1)),
        ("checklist", UIColor(red: 0.96, green: 0.23, blue: 0.21, alpha: 1)),
        ]
    
    
 

    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        projectCollectionView.delegate = self
        projectCollectionView.dataSource = self

//
//        let layout = UICollectionViewFlowLayout()
//        layout.itemSize = CGSize(width: 100, height: 100)
//        layout.minimumInteritemSpacing = 8
//        layout.minimumLineSpacing = 8
//        layout.headerReferenceSize = CGSize(width: 0, height: 40)
//        layout.sectionInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
//        projectCollectionView.collectionViewLayout = layout

     // maxX = 414     348 832     192
        // Add button circle menu
        
       
      let  button = CircleMenu(
            frame: CGRect(x: view.frame.maxX - 70, y: view.frame.maxY - 150, width: 50, height: 50),
            normalIcon:"addsuperwhite",
            selectedIcon:"icon_close",
            buttonsCount: 4,
            duration: 4,
            distance: 120)
        
        button.backgroundColor = UIColor.white
        button.distance = 100
        button.startAngle = -90
        button.endAngle = 0
        button.duration = 0.25
        button.buttonsCount = 3
        button.delegate = self
        button.backgroundColor = UIColor(hexString: "A569F5")
        button.layer.cornerRadius = button.frame.size.width / 2.0
        view.addSubview(button)
        
        
        
        projectCollectionView.isScrollEnabled = true
        projectCollectionView.isPagingEnabled = true
    
    navigationController?.navigationBar.titleTextAttributes = textAttributes
    
        // Change the alignment to left
        
        todayBtn.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        upcomingBtn.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        projectBtn.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        inBoxBtn.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        
        
        // Register cell
        
        self.todayTableView.register(UINib(nibName: "ToDoCell", bundle: nil), forCellReuseIdentifier: toDoCellIdentifier)
         self.upcomingTableView.register(UINib(nibName: "ToDoCell", bundle: nil), forCellReuseIdentifier: toDoCellIdentifier)
         self.inboxTableView.register(UINib(nibName: "ToDoCell", bundle: nil), forCellReuseIdentifier: toDoCellIdentifier)

        
        todayTableView.dataSource = self
        todayTableView.delegate = self
        
        upcomingTableView.delegate = self
        upcomingTableView.dataSource = self
        
        inboxTableView.delegate = self
        inboxTableView.dataSource = self
        
        todayTableView.emptyDataSetSource = self
        todayTableView.emptyDataSetDelegate = self
        
        upcomingTableView.emptyDataSetDelegate = self
        upcomingTableView.emptyDataSetSource = self
        
        inboxTableView.emptyDataSetDelegate = self
        inboxTableView.emptyDataSetSource = self
        
        projectCollectionView.emptyDataSetDelegate = self
        projectCollectionView.emptyDataSetSource = self
        
        reloadRemoveAppend()
        
     
        
         generateSuccessQuote()
       
      
    }
    
 
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("egrwgf")
    }
    
    

    func updateNumberOfTask(){
        let numberOfTaskToday = todayList.count
        todayNumberOfTask.text = String(numberOfTaskToday)
        
        let inboxTask = String(inBoxList.count)
        InBoxNumberOfTask.text = inboxTask
        
        let upcomingTask = String(upComingList.count)
        upcomingNumberOfTask.text = upcomingTask
        
        
        let projectTask = String(projectList.count)
        projectNumberOfTask.text = projectTask
    }
    
    
    @IBAction func todayAddTask(_ sender: Any) {
        presentAddVC(atIndex: 2, today: true)
        
        
    }
    
    @IBAction func projectAddTask(_ sender: Any) {
        presentAddVC(atIndex: 1, today: false)
    }
    
    
    @IBAction func inBoxAddTask(_ sender: Any) {
        presentAddVC(atIndex: 2, today: false)
    }
    
    
    @IBAction func upcomingAddTask(_ sender: Any) {
        presentAddVC(atIndex: 2, today: false)
    }
    
    
    
    func presentAddVC(atIndex : Int,today : Bool){
        guard let addVC = storyboard?.instantiateViewController(withIdentifier: "AddVC") as? AddVC else{ return}
        
        
        let transitionDelegate = SPStorkTransitioningDelegate()
        
        transitionDelegate.storkDelegate = self
        
        addVC.transitioningDelegate = transitionDelegate
        
        addVC.modalPresentationStyle = .custom
        
        
        switch atIndex {
        case 0:
            addVC.showSwitch = 0
            addVC.color = FlatBlue()
            
        case 1:
            addVC.showSwitch = 1
            addVC.color = FlatGreen()
           
        case 2:
            addVC.showSwitch = 2
            addVC.color = FlatRed()
            
            if today {
                addVC.dateSelected = dateFormatter.string(from: Date())
           
            }
        default:
            addVC.color = FlatGreen()
        }
        
        self.present(addVC, animated: true, completion: nil)
    }

 
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
     reloadRemoveAppend()

      reloadDataForAll()
        
   
    }
    
    
    func reloadAppendWithCondition(){
        
    }
    
    func reloadDataForAll(){
    todayTableView.reloadData()
    upcomingTableView.reloadData()
    inboxTableView.reloadData()
    projectCollectionView.reloadData()
    }
    
    func reloadRemoveAppend(){
        
       
        
        removeEverything()
        
        
        AppendToTodayAndUpcoming()
        
        fetchAppendProject()
        
   
        reloadEverything()
        
        updateNumberOfTask()
    }
    

    func removeEverything(){
        todayList.removeAll()
        upComingList.removeAll()
        inBoxList.removeAll()
        
        projectList.removeAll()
        projectToPopulate.removeAll()
    
    }
    
    func reloadEverything(){
        todayTableView.reloadData()
        upcomingTableView.reloadData()
        inboxTableView.reloadData()
        
        projectCollectionView.reloadData()
    }
    
    @IBAction func todayBtnTapped(_ sender: Any) {
        performSegue(withIdentifier: goToTopicControllerIdentifier, sender: 1)
    }
    
    @IBAction func projectNameBtnTaped(_ sender: Any) {
        performSegue(withIdentifier: goToTopicControllerIdentifier, sender: 2)
    }
    
    @IBAction func upcomingBtnTapped(_ sender: Any) {
           performSegue(withIdentifier: goToTopicControllerIdentifier, sender: 3)
       
    }

    @IBAction func inBoxBtnTapped(_ sender: Any) {
         performSegue(withIdentifier: goToTopicControllerIdentifier, sender: 4)
    }
    
    
    
    @IBAction func eventBtnTapped(_ sender: Any) {
        performSegue(withIdentifier: "goToEvent", sender: self)
        
    }
    
    
    func appendToProjectToPopulate(){
        var i = 0
        
        while i < projectList.count{
            if let projectName =  projectList[i].name,let projectDate = projectList[i].date,let hexString = projectList[i].hexString {
                fetchFromProject(nameProject: projectName, date: projectDate, hexString: hexString)
            }
            i += 1
        }
    }
    
    
    func fetchFromProject(nameProject : String,date : String,hexString : String){
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
            request.predicate = NSPredicate(format: "%K = %@", "project", "\(nameProject)")
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coreDataToDos = try? context.fetch(request) as? [Task] {
                if let theToDos = coreDataToDos {
                    
                    for data in theToDos {
                        if data.done {
                            doneTask.append(data)
                        }
                        
                        taskChild.append(data)
                        
                    }
                    projectToPopulate.append(projectData(nameProject: nameProject, date: date, taskChild: taskChild, doneTask: doneTask, hexString: hexString))
                    
                    taskChild.removeAll()
                    doneTask.removeAll()
                    
                }
            }
        }
    }
    
    
    func appendToProjectList(){
        
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Project")
//            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coreDataToDos = try? context.fetch(request) as? [Project] {
                if let theToDos = coreDataToDos {
                    
                    for data in theToDos {
                        
                     projectList.append(data)
                        
                        
                    }
            
                    
                }
            }
        }
    }
    
    
    func AppendToTodayAndUpcomingWithCondition(inInBoxList : Bool,inTodayList : Bool, inUpComingList : Bool) {
        let currentDate = dateFormatter.string(from: date)
        
        let currentDateToCompare = dateFormatter.date(from: currentDate)
        
        
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coreDataToDos = try? context.fetch(request) as? [Task] {
                if let theToDos = coreDataToDos {
                    for data in theToDos {
                        if inInBoxList{
                             inBoxList.append(data)
                            }
                        else if inTodayList{
                            if let dateToCompare = dateFormatter.date(from: data.date!) {
                                if (currentDateToCompare?.isEqualTo(dateToCompare))!{
                                    todayList.append(data)
                                }else if (currentDateToCompare?.isSmallerThan(dateToCompare))!{
//                                    upComingList.append(data)
                                }
                            }
                        }else if inUpComingList{
                            
                            if let dateToCompare = dateFormatter.date(from: data.date!) {
                                if (currentDateToCompare?.isEqualTo(dateToCompare))!{
//                                    todayList.append(data)
                                }else if (currentDateToCompare?.isSmallerThan(dateToCompare))!{
                                    upComingList.append(data)
                                }
                            }
                        }
                    }
                }
            }
        }
    
    }

    func AppendToTodayAndUpcoming() {
        let currentDate = dateFormatter.string(from: date)
        
        let currentDateToCompare = dateFormatter.date(from: currentDate)
       
        
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
            request.sortDescriptors = [NSSortDescriptor(key: "done", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coreDataToDos = try? context.fetch(request) as? [Task] {
                if let theToDos = coreDataToDos {
                    
                    for data in theToDos {
                        
                        inBoxList.append(data)
                        
                        if let dateToCompare = dateFormatter.date(from: data.date!) {
                            if (currentDateToCompare?.isEqualTo(dateToCompare))!{
                                todayList.append(data)
                            }else if (currentDateToCompare?.isSmallerThan(dateToCompare))!{
                                upComingList.append(data)
                            }
                        }
                  
                       
                    }
                
                    
                }
            }
        }
    }
    
    

}


extension toDoListVC : SPStorkControllerDelegate{
    func didDismissStorkByTap() {
    
        reloadRemoveAppend()
        
        updateNumberOfTask()
    }
    func didDismissStorkBySwipe() {
        reloadRemoveAppend()
        
        updateNumberOfTask()
      
    }
   
    
    
}

extension toDoListVC : UITableViewDelegate,UITableViewDataSource, YourCellDelegate{
   
    func inboxRemoveAndReload(){
        self.inBoxList.removeAll()
        self.AppendToTodayAndUpcomingWithCondition(inInBoxList: true, inTodayList: false, inUpComingList: false)
        self.inboxTableView.reloadData()
    }
    func upComingRemoveAndReload(){
        self.upComingList.removeAll()
        self.AppendToTodayAndUpcomingWithCondition(inInBoxList: false, inTodayList: false, inUpComingList: true)
        self.upcomingTableView.reloadData()
    }
    func todayRemoveAndReload(){
        self.todayList.removeAll()
          self.AppendToTodayAndUpcomingWithCondition(inInBoxList: false, inTodayList: true, inUpComingList: false)
        self.todayTableView.reloadData()
    }
    
    func fetchAppendProject(){
        self.projectToPopulate.removeAll()
        self.projectList.removeAll()
        
        appendToProjectList()
        
        appendToProjectToPopulate()
    }
    
    func didPressButton(_ tableView: UITableView) {
        print("This is table view tag\(tableView.tag)")
        if tableView.tag == 0{
            inboxRemoveAndReload()
            
            fetchAppendProject()
            projectCollectionView.reloadData()
        }else if tableView.tag == 1 {
            upComingRemoveAndReload()
            todayRemoveAndReload()
        
            fetchAppendProject()
             projectCollectionView.reloadData()
        }else if tableView.tag == 2 {
            inboxRemoveAndReload()
            
            fetchAppendProject()
             projectCollectionView.reloadData()
        }
    }
    
    
    
    
    
    func createCell(nameIdentifier : String,indexPath : IndexPath) -> UITableViewCell{
        
        if nameIdentifier == "today" {
            guard let cell = todayTableView.dequeueReusableCell(withIdentifier: toDoCellIdentifier, for: indexPath) as? ToDoCell else {  return UITableViewCell()}
            if let name = todayList[indexPath.row].name, let time = todayList[indexPath.row].time, let hexString = todayList[indexPath.row].hexString,let projectName = todayList[indexPath.row].project  {
                
                cell.configureCell(taskLb: name, doneCheck: todayList[indexPath.row].done, time: time, project: projectName, date: "", hexString: hexString, important: todayList[indexPath.row].important )
                cell.btnImageUndone.tag = todayTableView.tag
                cell.cellDelegate = self
               
                
                
                  return cell
            }
        }else if nameIdentifier == "inbox"{
            guard let cell = inboxTableView.dequeueReusableCell(withIdentifier: toDoCellIdentifier, for: indexPath) as? ToDoCell else {  return UITableViewCell()}
            
            if let name = inBoxList[indexPath.row].name, let time = inBoxList[indexPath.row].time,let dateTo = inBoxList[indexPath.row].date, let hexString = inBoxList[indexPath.row].hexString, let projectName = inBoxList[indexPath.row].project  {
            
                cell.configureCell(taskLb: name, doneCheck: inBoxList[indexPath.row].done, time: time, project: projectName, date: dateTo, hexString: hexString, important: inBoxList[indexPath.row].important )

                cell.btnImageUndone.tag = inboxTableView.tag
                cell.cellDelegate = self
               
               
                
                return cell
            }
        }else if nameIdentifier == "upcoming"{
            
            guard let cell = upcomingTableView.dequeueReusableCell(withIdentifier: toDoCellIdentifier, for: indexPath) as? ToDoCell else {  return UITableViewCell()}
            if let name = upComingList[indexPath.row].name, let time = upComingList[indexPath.row].time,let dateTo = upComingList[indexPath.row].date , let hexString = upComingList[indexPath.row].hexString,let projectName = upComingList[indexPath.row].project  {
                
                cell.configureCell(taskLb: name, doneCheck: upComingList[indexPath.row].done, time: time, project: projectName, date: dateTo, hexString: hexString, important: upComingList[indexPath.row].important )

                cell.btnImageUndone.tag = upcomingTableView.tag
                cell.cellDelegate = self
               
               
                
                return cell
            }
        }
   
       
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == todayTableView {
            return todayList.count
        }else if tableView == upcomingTableView {
            return upComingList.count
        }else if tableView == inboxTableView {
             return inBoxList.count
        }
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == todayTableView {
          let cell = createCell(nameIdentifier: "today", indexPath: indexPath)
            return cell
            
        }else if tableView == upcomingTableView {
            
            let cell = createCell(nameIdentifier: "upcoming", indexPath: indexPath)
            return cell
            
        }else if tableView == inboxTableView {
            let cell = createCell(nameIdentifier: "inbox", indexPath: indexPath)
            return cell
        }
        
      
        return UITableViewCell()
      
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        
        if tableView == todayTableView {
           
            
          seletedItemInTableView = todayList[indexPath.row]
        
            
        }else if tableView == inboxTableView {
          
             seletedItemInTableView = inBoxList[indexPath.row]
           
        }else if tableView == upcomingTableView{
            
           seletedItemInTableView = upComingList[indexPath.row]
         
        }
        
        
        performSegue(withIdentifier: "goToEdit", sender: seletedItemInTableView)
        
        
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if tableView == todayTableView{
        
        let deleteAction = removeRow(tableViewIdentifier: "today")
        return deleteAction
        }else if tableView == inboxTableView {
    let deleteAction = removeRow(tableViewIdentifier: "inbox")
    return deleteAction
        }else if tableView == upcomingTableView{
    let deleteAction = removeRow(tableViewIdentifier: "upcoming")
    return deleteAction
        }
        
        return nil
    }
    
    func removeRow(tableViewIdentifier : String) -> [UITableViewRowAction]{
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") { (rowAction, indexPath) in
            
            if tableViewIdentifier == "today" {
                if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
                    
                    context.delete(self.todayList[indexPath.row])
                    
                    try? context.save()
                    
                     self.cancelNotification(task: self.todayList[indexPath.row])
                  
                    self.todayList.remove(at: indexPath.row)
                    self.todayTableView.deleteRows(at: [indexPath], with: .fade)
                    self.reloadRemoveAppend()
                    
                }
            }else if tableViewIdentifier == "inbox"{
                if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
                    
                    context.delete(self.inBoxList[indexPath.row])
                    
                    try? context.save()
                    
                     self.cancelNotification(task: self.inBoxList[indexPath.row])
                     
                    self.inBoxList.remove(at: indexPath.row)
                    self.inboxTableView.deleteRows(at: [indexPath], with: .fade)
                    
                    self.reloadRemoveAppend()
                }
            }else if tableViewIdentifier == "upcoming" {
                if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
                    
                    context.delete(self.upComingList[indexPath.row])
                    
                    try? context.save()
                    
                    self.cancelNotification(task: self.upComingList[indexPath.row])
                    
                    self.upComingList.remove(at: indexPath.row)
                    self.upcomingTableView.deleteRows(at: [indexPath], with: .fade)
                     self.reloadRemoveAppend()
                }
            }
          
        }
  
        
        return [deleteAction]
    }
    
    func cancelNotification( task : Task){
        if let name = task.name {
              appDelegate?.stopLocalNotification(identifier: name)
        }
    }
    

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == todayTableView {
            return 68
        }else if tableView == upcomingTableView {
            return 68
        }else if tableView == inboxTableView {
            return 68
        }
        return 68
    }
}


extension toDoListVC : UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return projectToPopulate.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = projectCollectionView.dequeueReusableCell(withReuseIdentifier: "projectCell", for: indexPath) as? ProjectCollectionCell else {return UICollectionViewCell()}
        
        let name =  projectToPopulate[indexPath.row].nameProject
        
        let date = projectToPopulate[indexPath.row].date
        
        let numberOfTask = projectToPopulate[indexPath.row].taskChild.count
        
        let hexString = projectToPopulate[indexPath.row].hexString
       
        if numberOfTask == 0{
            
            cell.cofigureCell(project: name, numberOfTaskString: "\(numberOfTask) tasks", progress: 0, dueDate: date)
            let view = UIView(frame: cell.bounds)
            // Set background color that you want
          
//        view.backgroundColor = UIColor(hexString: hexString)?.lighten(byPercentage: 10)
            view.backgroundColor = UIColor(hexString: hexString)
            
            cell.layer.borderColor = UIColor.black.cgColor
            cell.layer.borderWidth = 3
            cell.layer.cornerRadius = 20 // optional
            
//           cell.configureColorCell(color: FlatBlack())
            
               cell.configureColorCell(color:ContrastColorOf(UIColor(hexString: hexString), returnFlat: true) )
            
            cell.backgroundView = view
            return cell
        }else{
            
            let projectToUser =  projectToPopulate[indexPath.row]
            
            let projectDoneTaskCount = Float(projectToUser.doneTask.count)
            let projectTaskCount = Float(projectToUser.taskChild.count)
            let divide = projectDoneTaskCount/projectTaskCount
            cell.cofigureCell(project: name, numberOfTaskString: "\(numberOfTask) tasks", progress: divide, dueDate: date)
            let view = UIView(frame: cell.bounds)
            // Set background color that you want
         
//                view.backgroundColor = UIColor(hexString: hexString)?.lighten(byPercentage: 10)
               view.backgroundColor = UIColor(hexString: hexString)
            
            
            cell.layer.borderColor = UIColor.black.cgColor
            cell.layer.borderWidth = 3
            cell.layer.cornerRadius = 20 // optional
            
            cell.configureColorCell(color:ContrastColorOf(UIColor(hexString: hexString), returnFlat: true) )
            
            cell.backgroundView = view
            return cell
        }
        

        
    }
    
    
  
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let projectToPopulateForData = projectToPopulate[indexPath.row]
        performSegue(withIdentifier: "goToProject", sender: projectToPopulateForData)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToProject" {
            guard let showProjectVC = segue.destination as? ShowProjectVC else {return}
            showProjectVC.projectToPoulateForThisView = sender as? projectData
         
            
        }else if segue.identifier == "goToEdit"{
            guard let editVC = segue.destination as? EditTaskVC else{return}
            if let taskToSend = sender as? Task {
                 editVC.selectedItem = taskToSend
            }
           
        }else if segue.identifier == goToTopicControllerIdentifier{
            guard let controllerVC = segue.destination as? TopicControllerForTodayAndEtc else {return}
            controllerVC.showSwitch = sender as! Int
            
        }
    }
    
   
    
    
}

extension toDoListVC : CircleMenuDelegate {
    func circleMenu(_: CircleMenu, willDisplay button: UIButton, atIndex: Int) {
        button.backgroundColor = items[atIndex].color
        
        button.setImage(UIImage(named: items[atIndex].icon), for: .normal)
        
        // set highlited image
        let highlightedImage = UIImage(named: items[atIndex].icon)?.withRenderingMode(.alwaysTemplate)
        button.setImage(highlightedImage, for: .highlighted)
        button.tintColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
    }
    
    func circleMenu(_: CircleMenu, buttonWillSelected _: UIButton, atIndex: Int) {
     
//       performSegue(withIdentifier: "goToAddVC", sender: self)
        guard let addVC = storyboard?.instantiateViewController(withIdentifier: "AddVC") as? AddVC else{ return}
        
        
        let transitionDelegate = SPStorkTransitioningDelegate()
        
        transitionDelegate.storkDelegate = self
        
        addVC.transitioningDelegate = transitionDelegate
        
        addVC.modalPresentationStyle = .custom

      
        switch atIndex {
        case 0:
            addVC.showSwitch = 0
            addVC.color = FlatBlue()
        case 1:
            addVC.showSwitch = 1
            addVC.color = FlatGreen()
        case 2:
            addVC.showSwitch = 2
            addVC.color = FlatRed()
        default:
            addVC.color = FlatGreen()
        }
        
        self.present(addVC, animated: true, completion: nil)
    }
    
    func circleMenu(_: CircleMenu, buttonDidSelected _: UIButton, atIndex: Int) {
        
    }

}





extension toDoListVC : EmptyDataSetSource,EmptyDataSetDelegate{
    
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        if scrollView == todayTableView {
             return image
        }else if scrollView == upcomingTableView{
             return image2
        }else if scrollView == inboxTableView{
             return image3
        }else if scrollView == projectCollectionView{
             return image4
        }
       return nil
    }
    
    func imageAnimation(forEmptyDataSet scrollView: UIScrollView) -> CAAnimation? {
        return imageAnimation
    }
    
//    func buttonTitle(forEmptyDataSet scrollView: UIScrollView, for state: UIControl.State) -> NSAttributedString? {
//        return NSAttributedString(string: "Please add some")
//    }
//
//    func buttonBackgroundImage(forEmptyDataSet scrollView: UIScrollView, for state: UIControl.State) -> UIImage? {
//        return UIImage(named: "GroupSele")
//    }
//
//
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        if scrollView == todayTableView {
           return NSAttributedString(string: "Make each day your masterpiece.")
        }else if scrollView == upcomingTableView{
          return NSAttributedString(string: "Every moment is a fresh beginning.")
        }else if scrollView == inboxTableView{
          return NSAttributedString(string: "The day is what you make it! So why not make it a great one?")
        }else if scrollView == projectCollectionView{
           return NSAttributedString(string: "It is never too late to be what you might have been.")
        }
      return nil
        
    }
    
    func backgroundColor(forEmptyDataSet scrollView: UIScrollView) -> UIColor? {
       return UIColor.white
        
    }
    
    func emptyDataSet(_ scrollView: UIScrollView, didTapButton button: UIButton) {
        print("Tapp Buton")
    }
    
    func emptyDataSet(_ scrollView: UIScrollView, didTapView view: UIView) {
        print("Tappp View")
    }
    
    func verticalOffset(forEmptyDataSet scrollView: UIScrollView) -> CGFloat {
        
        return 15
    }
    
    func spaceHeight(forEmptyDataSet scrollView: UIScrollView) -> CGFloat {
        return 10
    }
}
