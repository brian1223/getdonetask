//
//  EventAndProjectEditVC.swift
//  GetDoneTask
//
//  Created by Thang Thoi Truong on 5/27/19.
//  Copyright © 2019 Thang Thoi Truong. All rights reserved.
//
import UIKit
import  CoreData
import DateTimePicker
import ChameleonFramework



class EventAndProjectEditVC: UIViewController {

    @IBOutlet weak var projectNameTextField: UITextField!
    
    @IBOutlet weak var timeLb: UILabel!
    
    @IBOutlet weak var dateLb: UILabel!
    
    @IBOutlet weak var importantBtn: UIButton!
    
    @IBOutlet weak var colorShowLb: UIView!
    
    var selectedEvent  : Event?
    
    var selectedProject  : Project?
    
    var projectBool = false
    
    var important = false
    
    var timeSelected = String()
    
    var dateSelected = String()
    
    var picker = DateTimePicker()
    
    var hexString = String()
    
    var color = FlatBlack()

    var delegate : MySelectedProjectDelegateProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
   
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if selectedEvent != nil {
            // Event
            
            if let name = selectedEvent?.name {
                projectNameTextField.text = name
            }
            if let dateselected = selectedEvent!.date,let timeselected = selectedEvent!.time {
                dateSelected = dateselected
                timeSelected = timeselected
            }
            
           
            if dateSelected != "" {
                dateLb.text = " Date : \(dateSelected)"
            }else{
                dateLb.text = " Date : Undefined"
            }
            if timeSelected != "" {
                timeLb.text =  " Time : \(timeSelected )"
            }else {
                timeLb.text =  " Time : Undefined"
            }
            
            if let hexString = selectedEvent!.hexString {
                self.hexString = hexString
            }
            if let important2 = selectedEvent?.important {
                important = important2
            }
             importantButtonState()
             colorLbShow()
            
        }
        
        if selectedProject != nil{
            // Project
            
            if let name = selectedProject?.name {
                projectNameTextField.text = name
            }
            if let dateselected = selectedProject!.date,let timeselected = selectedProject!.time {
                dateSelected = dateselected
                timeSelected = timeselected
            }
            
            
            if dateSelected != "" {
                dateLb.text = " Date : \(dateSelected)"
            }else{
                dateLb.text = " Date : Undefined"
            }
            if timeSelected != "" {
                timeLb.text =  " Time : \(timeSelected )"
            }else {
                timeLb.text =  " Time : Undefined"
            }
            
            if let hexString = selectedProject!.hexString {
                self.hexString = hexString
            }
            
            if let important2 = selectedProject?.important {
                important = important2
            }
             importantButtonState()
          colorLbShow()
            
        }
    
        print("HEX STRING \(hexString)")
    }
    
    func colorLbShow(){
        if hexString == "#ffffff" {
            
            
            colorShowLb.backgroundColor = FlatWhite()
        }else{
            colorShowLb.backgroundColor = UIColor(hexString: hexString)
        }
        
    }
    @IBAction func importantBtnTapped(_ sender: Any) {
        if important {
            important = false
            importantButtonState()
        }else{
            important = true
            importantButtonState()
        }
    }
    @IBAction func dateChangeTapped(_ sender: Any) {
           performSegue(withIdentifier: "goToCalendar", sender: nil)
    }
    
    
    @IBAction func timeChangeTapped(_ sender: Any) {
         createCalendar(isDatePicker: false)
    }
    
    
    @IBAction func changeColorTapped(_ sender: Any) {
          performSegue(withIdentifier: "toTagColorVC", sender: self)
        
        
    }
    
    
    
    @IBAction func editTapped(_ sender: Any) {
        if selectedProject != nil{
            // project
            guard let naemPro = selectedProject?.name else {return}
            print("NamePro\(naemPro)")
            guard  let project =  fetchTask(nameProject: naemPro, project: true) else {return}
            print("Project : \(project)")
            if delegate != nil{
                delegate?.sendItem(myData: project)
            }
             navigationController?.popViewController(animated: true)
          
        }
        if selectedEvent != nil{
            guard let naemPro = selectedEvent?.name else {return}
        
            fetchEvent(nameProject: naemPro)
             navigationController?.popViewController(animated: true)
        }
        
        

    }
    
    @IBAction func deleteTaped(_ sender: Any) {
        
        if selectedProject != nil{
            // project
            guard let naemPro = selectedProject?.name else {return}
       
           deleteTask(nameProject: naemPro, project: true)
            
            
        }
        if selectedEvent != nil{
            guard let naemPro = selectedEvent?.name else {return}
            
            deleteTask(nameProject: naemPro, project: false)
        }
           navigationController?.popToRootViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToCalendar" {
            guard let chooseDateVC = segue.destination as? ChooseDateAndTimeVC else {return}
            
            
            chooseDateVC.color = self.color
  
            chooseDateVC.delegateEvent  = self
            if selectedEvent != nil{
                chooseDateVC.selectedEvent = selectedEvent!
            }
         
            chooseDateVC.delegateProject  = self
            
            if selectedProject != nil{
                  chooseDateVC.selectedProject = selectedProject!
            }
          
        }
        
        if segue.identifier == "toTagColorVC"{
            guard let chooseColorVC = segue.destination as? tagColorVC else {return}
            chooseColorVC.delegateProjectColor = self
              chooseColorVC.delegateEventColor = self
            chooseColorVC.selectedEvent = self.selectedEvent
             chooseColorVC.selectedProject = self.selectedProject
        }
        
        if segue.identifier == "goToShowProject"{
            guard let editVC = segue.destination as? ShowProjectVC else {return}
            editVC.selectedProject = sender as? Project
        }
    }
    
    func importantButtonState(){
        if important == false {
            importantBtn.setTitle("✩", for: .normal)
            
        }else{
            importantBtn.setTitle("★", for: .normal)
        }
        
    }
    
    func createCalendar(isDatePicker : Bool){
        let min = Date()
        //            .addingTimeInterval(-60 * 60 * 24 * 4)
        let max = Date().addingTimeInterval(480 * 60 * 24 * 4)
        picker = DateTimePicker.create(minimumDate: min, maximumDate: max)
        
        //                picker.dateFormat = "hh:mm aa dd/MM/YYYY"
        if isDatePicker {
            picker.dateFormat = "dd/MM/YYYY"
            picker.isDatePickerOnly = true
        }else{
            picker.dateFormat = "h:mm a"
            picker.is12HourFormat = true
            picker.todayButtonTitle = ""
            picker.isTimePickerOnly = true
        }
        
        
        picker.includeMonth = true // if true the month shows at bottom of date cell
        picker.highlightColor = color
        picker.darkColor = UIColor.darkGray
        picker.doneButtonTitle = "Save time"
        picker.doneBackgroundColor = color
        
        if isDatePicker {
            picker.completionHandler = { date in
                
                let formatter = DateFormatter()
                formatter.dateFormat = "dd/MM/YYYY"
                self.dateLb.text = formatter.string(from: date)
                
            }
        }else{
            picker.completionHandler = { date in
                
                let formatter = DateFormatter()
                formatter.dateFormat = "h:mm a"
                formatter.amSymbol  = "AM"
                formatter.pmSymbol = "PM"
                
                self.timeSelected = formatter.string(from: date)
                self.timeLb.text = "Time : \(self.timeSelected)"
                
            }
        }
        
        //         add tap gesture to dismiss
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissDateAndTime))
        tap.cancelsTouchesInView = true
        tap.numberOfTapsRequired = 2
        self.picker.addGestureRecognizer(tap)
        
        picker.delegate = self
        picker.show()
    }
    
    
    @objc func dismissDateAndTime(){
        picker.dismissView()
    }
    
    
    func fetchTask(nameProject : String,project: Bool) -> Project?{
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            
            // Save Task Under Project
            saveDataTaskUnderProject(nameProject: nameProject, context: context)
            
            // Save Data For Project
               return  saveData( nameProject: nameProject, context: context)
            }
    return nil
        }
    
    func fetchEvent(nameProject : String){
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
          saveEvent(nameEntity: "Event", nameProject: nameProject, context: context)
            
        }
    }
    func saveData(nameProject : String, context : NSManagedObjectContext) -> Project?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Project")
        request.predicate = NSPredicate(format: "%K = %@", "name", "\(nameProject)")
        request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
        request.returnsObjectsAsFaults = false
        

            if let coreDataToDos = try? context.fetch(request) as? [Project] {
                if let theToDos = coreDataToDos {
                    
                    for data in theToDos {
                        if let nameOfTask = projectNameTextField.text {
                            
                            
                            data.name = nameOfTask
                            data.date = dateSelected
                            data.time = timeSelected
                            data.important = important
                            data.hexString = hexString
                            data.done = false
                            
                        }
                        
                        try? context.save()
                        
                      return data
                    }
                    
                    
                }
            }
        return nil
    }
    
    func saveDataTaskUnderProject(nameProject : String, context : NSManagedObjectContext){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
        request.predicate = NSPredicate(format: "%K = %@", "project", "\(nameProject)")
        request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
        request.returnsObjectsAsFaults = false
        
        
        if let coreDataToDos = try? context.fetch(request) as? [Task] {
            if let theToDos = coreDataToDos {
                
                for data in theToDos {
                    if let nameOfTask = projectNameTextField.text {
                        
                        data.project = nameOfTask
                        
                    }
                    
                    try? context.save()
                
                }
                
                
            }
        }
   
    }
    
    
    func saveEvent(nameEntity : String, nameProject : String, context : NSManagedObjectContext){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: nameEntity)
        request.predicate = NSPredicate(format: "%K = %@", "name", "\(nameProject)")
        request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
        request.returnsObjectsAsFaults = false
        if let coreDataToDos = try? context.fetch(request) as? [Event] {
            if let theToDos = coreDataToDos {
                
                for data in theToDos {
                    if let nameOfTask = projectNameTextField.text {
                        
                        
                        data.name = nameOfTask
                        data.date = dateSelected
                        data.time = timeSelected
                        data.important = important
                        data.hexString = hexString
                        data.done = false
                        
                    }
                    
                    try? context.save()
                    
                    
                }
                
                
            }
        }
    }
    func deleteTask(nameProject : String,project : Bool){
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            var nameEntity = String()
            if project {
                nameEntity = "Project"
             deleteTaskAndFetch(nameEntity: nameEntity, nameProject: nameProject, context: context)
            }else{
                  nameEntity = "Event"
            deleteTaskAndFetch(nameEntity: nameEntity, nameProject: nameProject, context: context)
            }

        }
    }
    
    
    func deleteTaskAndFetch(nameEntity : String,nameProject : String, context : NSManagedObjectContext){
        if nameEntity == "Project"{
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: nameEntity)
            request.predicate = NSPredicate(format: "%K = %@", "name", "\(nameProject)")
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coreDataToDos = try? context.fetch(request) as? [Project] {
                if let theToDos = coreDataToDos {
                    
                    for data in theToDos {
                        
                        context.delete(data)
                        
                        try? context.save()
                        
                    }
                    
                    
                }
            }
        }else if nameEntity == "Event"{
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: nameEntity)
            request.predicate = NSPredicate(format: "%K = %@", "name", "\(nameProject)")
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coreDataToDos = try? context.fetch(request) as? [Event] {
                if let theToDos = coreDataToDos {
                    
                    for data in theToDos {
                        
                        context.delete(data)
                        
                        try? context.save()
                        
                    }
                    
                    
                }
            }
        }
  
    }
}

extension EventAndProjectEditVC : MySelectedProjectDelegateProtocol{
    func sendItem(myData: Project) {
        selectedProject  = myData
    }
    
    
}
extension EventAndProjectEditVC : MyTaskColorProjectSendingProtocol{
    func sendProject(myData: Project) {
        selectedProject = myData
    }
    
    
}

extension EventAndProjectEditVC : MySelectedEventDelegateProtocol{
    func sendItem(myData: Event) {
        selectedEvent = myData
    }
    
    
}

extension EventAndProjectEditVC : MyTaskColorEventSendingProtocol{
    func sendEvent(myData: Event) {
        selectedEvent = myData
    }
    
    
}




extension EventAndProjectEditVC : DateTimePickerDelegate{
    func dateTimePicker(_ picker: DateTimePicker, didSelectDate: Date) {
        
    }
    
    
}


