//
//  ShowProjectVC.swift
//  GetDoneTask
//
//  Created by Thang Thoi Truong on 5/27/19.
//  Copyright © 2019 Thang Thoi Truong. All rights reserved.
//

import UIKit
import CoreData
import ChameleonFramework
import SCLAlertView

let goToEditAndEventSegue = "goToEditEventAndProject"

class ShowProjectVC: UIViewController, UITextFieldDelegate,YourCellDelegate {
   
    
    
    @IBOutlet weak var projectLb: UILabel!
    
    @IBOutlet weak var dueDateLb: UILabel!
    
    @IBOutlet weak var numberOfTask: UILabel!
    
    @IBOutlet weak var taskUnderProjectTableView: UITableView!
    
    
    @IBOutlet weak var taskTextField: UITextField!
    
    @IBOutlet weak var addBtn: UIButton!
    
    @IBOutlet weak var colorBtn: UIButton!
    
    @IBOutlet weak var buttonAdnTextFieldView: UIView!
    
    @IBOutlet weak var progressViewShow: UIProgressView!
    
    
    
    
    var projectToPoulateForThisView : projectData!
    
    var taskChild = [Task]()
    
    var doneTask = [Task]()
    
    var hexString = String()
    

    var selectedProject : Project?
    
    var newTask = Task()
    
    var selectedItemToChoose = Task()
    
    var appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
  
        // color
        
        colorBtn.backgroundColor = FlatBlue()
        
        // Bind to Keyboard
//       taskTextField.bindToKeyboard()
//        addBtn.bindToKeyboard()
//        buttonAdnTextFieldView.bindToKeyboard()
        
        // table view
        taskUnderProjectTableView.delegate = self
        taskUnderProjectTableView.dataSource = self
        self.taskUnderProjectTableView.register(UINib(nibName: "ToDoCell", bundle: nil), forCellReuseIdentifier: toDoCellIdentifier)
        
 
        
        // Text Field
        
      taskTextField.delegate = self
        
        // Gesture Recognizer
        
        let down = UISwipeGestureRecognizer(target : self, action : #selector(self.swipeDown))
        down.direction = .down
        self.view.addGestureRecognizer(down)

        
        let up = UISwipeGestureRecognizer(target : self, action : #selector(self.swipeUp))
        up.direction = .up
        self.view.addGestureRecognizer(up)
        
        
        // populate the view
       
      
        
        // Progress
        
        
        setProgressViewAndLabel()
        
        progressViewShow.transform = progressViewShow.transform.scaledBy(x: 1, y: 4)
        progressViewShow.layer.cornerRadius = 8
        progressViewShow.clipsToBounds = true
        progressViewShow.layer.sublayers![1].cornerRadius = 8
        progressViewShow.subviews[1].clipsToBounds = true
        
//        print("This is count for tak child f\(projectToPoulateForThisView[0].taskChild.count)")
        
 
     
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        fetchAndAppend()
        print(projectToPoulateForThisView)
        if selectedProject != nil{
            guard let name =  selectedProject?.name else {return}
    
            fetchTaskProject(nameProject: name)
            
            self.setProgressViewAndLabel()
            taskUnderProjectTableView.reloadData()
        }else{
            fetchTaskProject(nameProject: projectToPoulateForThisView.nameProject)
        
            self.setProgressViewAndLabel()
            taskUnderProjectTableView.reloadData()
        }
       
    }
    


    
    @objc func didSetProgress(){
          guard  let projectToUser =  projectToPoulateForThisView else {return}
         let progress = Float((projectToUser.doneTask.count)/(projectToUser.taskChild.count))
        progressViewShow.progress = progress
    }
    
    func didPressButton(_ tableView: UITableView) {
        
//        fetchAndAppend()
        if selectedProject != nil{
            guard let name = selectedProject?.name else {return}
             fetchTaskProject(nameProject: name)
        }else{
              fetchTaskProject(nameProject: projectToPoulateForThisView.nameProject)
        }
      
        setProgressViewAndLabel()

        
    }
    
    func setProgressViewAndLabel(){
          guard  let projectToUser =  projectToPoulateForThisView else {return}
        let colorToshow =  UIColor(hexString: projectToUser.hexString)
        
        numberOfTask.text = "\(projectToUser.doneTask.count)/\(projectToUser.taskChild.count)"
        
        let projectDoneTaskCount = Float(projectToUser.doneTask.count)
        let projectTaskCount = Float(projectToUser.taskChild.count)
        let divide = projectDoneTaskCount/projectTaskCount
        
        progressViewShow.progress = divide
        if projectToUser.hexString == UIColor.white.hexValue(){
             progressViewShow.progressTintColor = FlatBlack()
        }else{
             progressViewShow.progressTintColor = colorToshow
        }
        
        projectLb.text = projectToUser.nameProject
        dueDateLb.text = projectToUser.date
        
        colorBtn.backgroundColor = colorToshow
        
    }

    

    
    @IBAction func addBtnTapped(_ sender: Any) {
//          let index = 0
    
        saveDataTaskForAddBtnAndReturn(textFieldReturn: false)
        
    }
    
    func saveDataTaskForAddBtnAndReturn(textFieldReturn : Bool){
        if (taskTextField.text?.isEmpty)! {
            if textFieldReturn {
                
            }else{
                SCLAlertView().showError("Misinformation", subTitle: "Please provide a name")
            }
            
        }else{
            saveDataTask()
            
            insertNewTask(task: newTask)
            
            setProgressViewAndLabel()
        }
    }
    
    @IBAction func deleteTapped(_ sender: Any) {
          guard  let projectToUser =  projectToPoulateForThisView else {return}
        
        let projectName = projectToUser.nameProject
        
        fetchFromProjectNameAndDeleteTaskAndProject(nameProject: projectName)
        
        
      navigationController?.popViewController(animated: true)
        
        
    }
    
    
    @IBAction func editTapped(_ sender: Any) {
        guard  let projectToUser =  projectToPoulateForThisView else {return}
        
        let projectName = projectToUser.nameProject
        
        
        guard let projectSent =  fetchTaskProjectWithNameAndReturn(nameProject: projectName) else {return}
        
        performSegue(withIdentifier: "goToEditEventAndProject", sender: projectSent)
    }
    
    
    func insertNewTask(task : Task){
         guard  let projectToUser =  projectToPoulateForThisView else {return}

          let indexPath = IndexPath(row: projectToUser.taskChild.count - 1, section: 0)
        
//         let indexPath = IndexPath(row: 0, section: 0)
 
        taskUnderProjectTableView.beginUpdates()
        taskUnderProjectTableView.insertRows(at: [indexPath], with: .left)
        taskUnderProjectTableView.endUpdates()
        
        
        taskTextField.text = ""
    }
 
    
    func saveDataTask(){
        guard  let projectToUser =  projectToPoulateForThisView else {return}
        
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            
            
            let entity = NSEntityDescription.entity(forEntityName: "Task", in: context)
            
            let toDo = NSManagedObject(entity: entity!, insertInto: context)
            
            if let titleText = taskTextField.text {
                toDo.setValue(titleText.capitalized, forKey: "name")
                toDo.setValue(false, forKey: "done")
                toDo.setValue(projectToUser.nameProject, forKey: "project")
                toDo.setValue("", forKey: "date")
                toDo.setValue("", forKey: "time")
                toDo.setValue(false, forKey: "important")
                toDo.setValue("#ffffff", forKey: "hexString")
                
                
                try? context.save()
                print("Save successfully into (Task)")
                
                projectToPoulateForThisView.taskChild.append(toDo as! Task)
                
//                   projectToPoulateForThisView[row].taskChild.insert(toDo as! Task, at: 0)
                
            }else{
                  SCLAlertView().showError("Misinformation", subTitle: "Please provide a name")
                
            }
        }

    }
    
    
    
    
    @objc func swipeDown(){
         view.endEditing(true)
    }
    @objc func swipeUp(){
        taskTextField.becomeFirstResponder()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
//        textField.resignFirstResponder()
        
        saveDataTaskForAddBtnAndReturn(textFieldReturn: true)
        
        
        return true
    }
    
    
    
    func fetchFromProjectNameAndDeleteTaskAndProject(nameProject : String){
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
            request.predicate = NSPredicate(format: "%K = %@", "project", "\(nameProject)")
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coreDataToDos = try? context.fetch(request) as? [Task] {
                if let theToDos = coreDataToDos {
                    
                    for data in theToDos {
                    context.delete(data)
                        
                    try? context.save()
                        
                    }
               
                    
                }
            }
        }
        
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Project")
            request.predicate = NSPredicate(format: "%K = %@", "name", "\(nameProject)")
            request.returnsObjectsAsFaults = false
            
            if let coreDataToDos = try? context.fetch(request) as? [Project] {
                if let theToDos = coreDataToDos {
                    
                    for data in theToDos {
                        context.delete(data)
                        
                        try? context.save()
                        
                    }
                    
                    
                }
            }
        }
    }
    
    
    func fetchTaskProject(nameProject : String){
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
            request.predicate = NSPredicate(format: "%K = %@", "project", "\(nameProject)")
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coreDataToDos = try? context.fetch(request) as? [Task] {
                if let theToDos = coreDataToDos {
                    
                    for data in theToDos {
                        if data.done {
                            doneTask.append(data)
                            
                        }
                        
                        taskChild.append(data)
                        
                    }
         
                    if selectedProject != nil {
                        guard let date = selectedProject?.date else {print("No Datefff"); return}
                        projectToPoulateForThisView = projectData(nameProject: nameProject, date: date, taskChild: taskChild, doneTask: doneTask, hexString: hexString)
                    }else{
                    projectToPoulateForThisView = projectData(nameProject: nameProject, date: projectToPoulateForThisView.date, taskChild: taskChild, doneTask: doneTask, hexString: hexString)
                    }
                    
        
                    
               
                    taskChild.removeAll()
                    doneTask.removeAll()
                    
                }
            }
        }
    }
    
    func fetchTaskProjectWithNameAndReturn(nameProject : String) -> Project?{
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Project")
            request.predicate = NSPredicate(format: "%K = %@", "name", "\(nameProject)")
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coreDataToDos = try? context.fetch(request) as? [Project] {
                if let theToDos = coreDataToDos {
                    
                    for data in theToDos {
                        return data
                        
                        
                    }
                }
            }
        }
        return nil
    }
    func fetchTaskProjectWithNameReTurnString(nameProject : String) -> String?{
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Project")
            request.predicate = NSPredicate(format: "%K = %@", "name", "\(nameProject)")
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coreDataToDos = try? context.fetch(request) as? [Project] {
                if let theToDos = coreDataToDos {
                    
                    for data in theToDos {
                        return data.name
                        
                        
                    }
                }
            }
        }
        return nil
    }
}
extension ShowProjectVC : UITableViewDelegate,UITableViewDataSource{
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        taskTextField.resignFirstResponder()
        if selectedProject != nil{
            guard let name = selectedProject?.name else {return}
            fetchTaskProject(nameProject: name)
        }else{
            fetchTaskProject(nameProject: projectToPoulateForThisView.nameProject)
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 
        return projectToPoulateForThisView.taskChild.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = taskUnderProjectTableView.dequeueReusableCell(withIdentifier: toDoCellIdentifier, for: indexPath) as? ToDoCell else {  return UITableViewCell()}

        let taskChild = projectToPoulateForThisView.taskChild[indexPath.row]
        let done = taskChild.done
//        let hexString = taskChild.hexString,
        
        if let name = taskChild.name, let dateToPass = taskChild.date,let time = taskChild.time {
            cell.configureCell(taskLb: name, doneCheck: done, time: time, project: "", date: dateToPass, hexString: "#ffffff", important: taskChild.important)
        cell.btnImageUndone.tag = indexPath.row
        cell.cellDelegate = self
            
            return cell
        }

        
        return UITableViewCell()
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let taskToDelete = projectToPoulateForThisView.taskChild[indexPath.row]
        
        if editingStyle == .delete {
            print("Deleted")
            
            if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
                
                context.delete(taskToDelete)
                
                try? context.save()
                
                self.projectToPoulateForThisView.taskChild.remove(at: indexPath.row)
                
                self.taskUnderProjectTableView.beginUpdates()
                self.taskUnderProjectTableView.deleteRows(at: [indexPath], with: .automatic)
                self.taskUnderProjectTableView.endUpdates()
                
                
             
                if selectedProject != nil{
                    guard let name = selectedProject?.name else {return}
                    fetchTaskProject(nameProject: name)
                }else{
                      fetchTaskProject(nameProject: projectToPoulateForThisView.nameProject)
                }
                self.setProgressViewAndLabel()
                
                self.cancelNotification(task: taskToDelete)
                
            }
            
   
        }
    }
    

    func cancelNotification( task : Task){
        if let name = task.name {
            self.appDelegate?.stopLocalNotification(identifier: name)
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let taskToEdit = projectToPoulateForThisView.taskChild[indexPath.row]
        print("This is gwgwgwegwgwrg Task To dit\(taskToEdit)")
     performSegue(withIdentifier: "goToEdit2", sender: taskToEdit)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToEdit2" {
            guard let editVC = segue.destination as? EditTaskVC else{return}
            if let taskToSend = sender as? Task {
                editVC.selectedItem = taskToSend
            }
            if segue.identifier == goToEditAndEventSegue {
                   guard let editVC = segue.destination as? EventAndProjectEditVC else{return}
                editVC.delegate = self
                
            }
    }
  
        if segue.identifier == goToEditAndEventSegue {
            guard let editVC = segue.destination as? EventAndProjectEditVC else{return}
            if let projectToSend = sender as? Project {
                editVC.selectedProject = projectToSend
                editVC.delegate = self
                
                // Delegate for event as well
                
            }
        }
    
}
}

extension ShowProjectVC : MySelectedProjectDelegateProtocol{
    func sendItem(myData: Project) {
        self.selectedProject = myData
    }
    
    
}
