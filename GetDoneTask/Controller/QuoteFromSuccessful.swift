//
//  QuoteFromSuccessful.swift
//  GetDoneTask
//
//  Created by Tien Thoi Truong on 8/6/19.
//  Copyright © 2019 Tien Thoi Truong. All rights reserved.
//

import Foundation
import SCLAlertView


var quoteSuccessArray = ["Success is not final; failure is not fatal: It is the courage to continue that counts","It is better to fail in originality than to succeed in imitation.","The road to success and the road to failure are almost exactly the same.", "Success usually comes to those who are too busy to be looking for it.","Opportunities don't happen. You create them.","Don't be afraid to give up the good to go for the great.","I find that the harder I work, the more luck I seem to have.","There are two types of people who will tell you that you cannot make a difference in this world: those who are afraid to try and those who are afraid you will succeed.", "Successful people do what unsuccessful people are not willing to do. Don't wish it were easier; wish you were better.","Try not to become a man of success. Rather become a man of value.","Never give in except to convictions of honor and good sense.","Stop chasing the money and start chasing the passion.","Success is walking from failure to failure with no loss of enthusiasm.","I owe my success to having listened respectfully to the very best advice, and then going away and doing the exact opposite.","If you are not willing to risk the usual, you will have to settle for the ordinary.", "The ones who are crazy enough to think they can change the world, are the ones that do.", "Do one thing every day that scares you.", "All progress takes place outside the comfort zone.","Don't let the fear of losing be greater than the excitement of winning.","If you really look closely, most overnight successes took a long time.","The only limit to our realization of tomorrow will be our doubts of today.","The way to get started is to quit talking and begin doing.", "The successful warrior is the average man, with laser-like focus.","There are no secrets to success. It is the result of preparation, hard work, and learning from failure.", "Success seems to be connected with action. Successful people keep moving. They make mistakes, but they don't quit.","If you really want to do something, you'll find a way. If you don't, you'll find an excuse.", "I cannot give you the formula for success, but I can give you the formula for failure--It is: Try to please everybody.", "Success is not the key to happiness. Happiness is the key to success. If you love what you are doing, you will be successful.", "Success isn't just about what you accomplish in your life; it's about what you inspire others to do.","Fall seven times and stand up eight.", "Some people dream of success while others wake up and work.", "If you can dream it, you can do it.","The difference between who you are and who you want to be is what you do.", "A successful man is one who can lay a firm foundation with the bricks that other throw at him.","In order to succeed, your desire for success should be greater than your fear of failure.","In order to succeed, we must first believe that we can.","Many of life's failures are people who did not realize how close they were to success when they gave up.","Don't be distracted by criticism. Remember--the only taste of success some people get is to take a bite out of you.", "The secret of success is to do the common thing uncommonly well.", "You know you are on the road to success if you would do your job, and not be paid for it.", "There is a powerful driving force inside every human being that, once unleashed, can make any vision, dream, or desire a reality.","The secret to success is to know something nobody else knows."]


var subtitleSuccessArrayFromWho = ["Winston S. Churchill"," Herman Melville","Colin R. Davis"," Henry David Thoreau"," Chris Grosser","John D. Rockefeller","Thomas Jefferson","Ray Goforth","Jim Rohn", "Albert Einstein","Winston Churchill","Tony Hsieh","Winston Churchill"," G. K. Chesterton","Jim Rohn","Anonymous","Anonymous","Michael John Bobak"," Robert Kiyosaki","Steve Jobs","Barack Obama","Franklin D. Roosevelt"," Walt Disney","Bruce Lee"," Colin Powell","Conrad Hilton","Jim Rohn","Herbert Bayard Swope"," Albert Schweitzer","Unknown","Japanese Proverb","Unknown","Walt Disney","Unknown","David Brinkley","Bill Cosby","Nikos Kazantzakis","Thomas Edison","Zig Ziglar","John D. Rockefeller Jr.","Oprah Winfrey"," Anthony Robbins","Aristotle Onassis"]

 let indx = Int.random(in: 0...(quoteSuccessArray.count - 1))

func generateSuccessQuote() {
   
    let indexx = Int.random(in: 1...6)
    
    if indexx == 1 {
         SCLAlertView().showNotice(quoteSuccessArray[indx], subTitle: subtitleSuccessArrayFromWho[indx])
    }else if indexx == 2{
         SCLAlertView().showEdit(quoteSuccessArray[indx], subTitle: subtitleSuccessArrayFromWho[indx])
    }else if indexx == 3{
         SCLAlertView().showWarning(quoteSuccessArray[indx], subTitle: subtitleSuccessArrayFromWho[indx])
    }else if indexx == 4{
        SCLAlertView().showSuccess(quoteSuccessArray[indx], subTitle: subtitleSuccessArrayFromWho[indx])
    }else if indexx == 5{
        SCLAlertView().showInfo(quoteSuccessArray[indx], subTitle: subtitleSuccessArrayFromWho[indx])
    }else if indexx == 6{
        SCLAlertView().showTitle(quoteSuccessArray[indx], subTitle: subtitleSuccessArrayFromWho[indx], style: .question)
    }
  
}


