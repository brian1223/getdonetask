//
//  TopicControllerForTodayAndEtc.swift
//  GetDoneTask
//
//  Created by Thang Thoi Truong on 5/27/19.
//  Copyright © 2019 Thang Thoi Truong. All rights reserved.
//


import UIKit
import ChameleonFramework
import CoreData
import SPStorkController
import ExpyTableView
import EmptyDataSet_Swift

let goToTopicControllerIdentifier = "goToTopicController"
let projectCellResuer = "projectResuseIdentifier"

let segueGoToTask = "goToTaskEdit"
let segueGoToShow = "goToShowProject"
let segueToAdd = "goToAddVC"
class TopicControllerForTodayAndEtc: UIViewController {

    @IBOutlet weak var topicLable: UILabel!
    
    @IBOutlet weak var tableViewForTopic: ExpyTableView!

    
    
    var list = [Any]()
    
    var showSwitch = Int()
    
    
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter
    }()
    
    fileprivate lazy var dateFormatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd"
        return formatter
    }()
    
    var todayList = [Task]()
    
    var upComingList = [Task]()
    
    var inBoxList = [Task]()
    
    var projectList = [Project]()
    
    var projectToPopulate = [projectData]()
    
    let date = Date()
    
    var taskChild = [Task]()
    
    var doneTask = [Task]()
    
    var i = 0
    
    var imageAnimation: CAAnimation? {
        let animation = CABasicAnimation.init(keyPath: "transform")
        animation.fromValue = NSValue.init(caTransform3D: CATransform3DIdentity)
        animation.toValue = NSValue.init(caTransform3D: CATransform3DMakeRotation(.pi/2, 0.0, 0.0, 1.0))
        animation.duration = 0.25
        animation.isCumulative = true
        animation.repeatCount = MAXFLOAT
        
        return animation;
    }
    
    var image: UIImage? {
        return UIImage.init(named: "champpp")
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableViewForTopic.delegate = self
        tableViewForTopic.dataSource = self
        
        tableViewForTopic.rowHeight = UITableView.automaticDimension
        tableViewForTopic.estimatedRowHeight = 68
        
        //Alter the animations as you want
        tableViewForTopic.expandingAnimation = .none
        tableViewForTopic.collapsingAnimation = .none
        
        tableViewForTopic.emptyDataSetSource = self
        tableViewForTopic.emptyDataSetDelegate = self
        
        
        tableViewForTopic.register(UINib(nibName: "ToDoCell", bundle: nil), forCellReuseIdentifier: toDoCellIdentifier)
        self.tableViewForTopic.register(UINib(nibName: "ProjectTableViewCell", bundle: nil), forCellReuseIdentifier: projectCellResuer)
 
    }
    
    func removeReload(){
        clearOutEverything()
        appendToeverthing()
        
        switch showSwitch {
        case 1:
            list = todayList
            topicLable.text = "Today List"
        case 2:
            list = projectToPopulate
            topicLable.text = "Project List"
            print(list)
            print(list.count)
        case 3:
            list = upComingList
            topicLable.text = "UpComing List"
        case 4:
            list = inBoxList
            topicLable.text = "InBox List"
        default:
            list = []
        }
        tableViewForTopic.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        removeReload()
        
    }
    
    @IBAction func addBtnTapped(_ sender: Any) {
        
//        performSegue(withIdentifier: segueToAdd, sender: self)
        
        createStorkToAdd()
        
    }
    
    @IBAction func deleteTapedAll(_ sender: Any) {
        
        switch showSwitch{
        case 1:
            // Today
            fetchAndDelete(inBox: false, today: true, upComing: false)
            
        case 2:
            // Project
            fetchProjectAndDelete()
        case 3:
            // UpComing
            fetchAndDelete(inBox: false, today: false, upComing: true)
            
        case 4:
            // InBOx
             fetchAndDelete(inBox: true, today: false, upComing: false)
           
        default:
            //
            fetchAndDelete(inBox: false, today: false, upComing: false)
        }
        
         navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueGoToTask{
            guard let editVC = segue.destination as? EditTaskVC else {return}
            editVC.selectedItem = sender as! Task
        }else if segue.identifier == segueGoToShow{
            guard let showVC = segue.destination as? ShowProjectVC else {return}
            showVC.selectedProject = sender as? Project
        }else if segue.identifier == segueToAdd{
            guard let addVC = segue.destination as? AddVC else {return}
            
            print(showSwitch)
            if self.showSwitch == 2 {
                addVC.showSwitch = 1
                addVC.color = FlatGreenDark()
            }else if showSwitch == 1{
                 addVC.showSwitch = 2
                addVC.dateSelected = dateFormatter.string(from: date)
                addVC.color = FlatRed()
            }else {
                addVC.showSwitch = 2
                addVC.color = FlatRed()
            }
        }
    }
    
    
    
    func createStorkToAdd(){
        guard let addVC = storyboard?.instantiateViewController(withIdentifier: "AddVC") as? AddVC else{ return}
        
        
        let transitionDelegate = SPStorkTransitioningDelegate()
        
        transitionDelegate.storkDelegate = self
        
        addVC.transitioningDelegate = transitionDelegate
        
        addVC.modalPresentationStyle = .custom
        if self.showSwitch == 2 {
            addVC.showSwitch = 1
            addVC.color = FlatGreenDark()
        }else if showSwitch == 1{
            addVC.showSwitch = 2
            addVC.dateSelected = dateFormatter.string(from: date)
            addVC.color = FlatRed()
        }else {
            addVC.showSwitch = 2
            addVC.color = FlatRed()
        }
        
        
        self.present(addVC, animated: true, completion: nil)
    }

}


extension TopicControllerForTodayAndEtc : SPStorkControllerDelegate{
    func didDismissStorkByTap() {
        removeReload()
      
    }
    func didDismissStorkBySwipe() {
       
        
    removeReload()
        
    }
    
    
    
}
extension TopicControllerForTodayAndEtc : ExpyTableViewDelegate,ExpyTableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if showSwitch == 2{
            guard let dataToPutIn = list as? [projectData] else {return UITableViewCell()}
            let arrayTask = dataToPutIn[indexPath.section].taskChild
            let dataRealTo = arrayTask[indexPath.row - 1 ]

            if arrayTask.count == 0{
                return UITableViewCell()
            }else{
                guard let cell = tableViewForTopic.dequeueReusableCell(withIdentifier: toDoCellIdentifier, for: indexPath) as? ToDoCell else {return UITableViewCell()}

                cell.configureCell(taskLb: dataRealTo.name!, doneCheck: dataRealTo.done, time: dataRealTo.time!, project: dataRealTo.project!, date: dataRealTo.date!, hexString: dataRealTo.hexString!, important: dataRealTo.important)
                return cell
            }


        }else{
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell {

        if showSwitch == 2 {
            guard let cell = tableViewForTopic.dequeueReusableCell(withIdentifier: projectCellResuer) as? ProjectTableViewCell else {return UITableViewCell() }
            guard let dataToPutIn = list as? [projectData] else {return UITableViewCell()}
            let realData = dataToPutIn[section]
            cell.cofigureCell(date: realData.date, time: "", projectName: realData.nameProject, colorString: realData.hexString)
            cell.layoutMargins = UIEdgeInsets.zero

            return cell

        }else {
            guard let cell = tableViewForTopic.dequeueReusableCell(withIdentifier: toDoCellIdentifier) as? ToDoCell else {return UITableViewCell()}
            guard let dataToPutIn = list[section] as? Task else {return UITableViewCell()}
            cell.configureCell(taskLb: dataToPutIn.name!, doneCheck: dataToPutIn.done, time: dataToPutIn.time!, project: dataToPutIn.project!, date: dataToPutIn.date!, hexString: dataToPutIn.hexString!, important: dataToPutIn.important)
            cell.layoutMargins = UIEdgeInsets.zero

                        return cell
        }

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let listToPutIn = list as? [projectData] else {return 1}
        if showSwitch == 2 {
            print("List To Put in : \(listToPutIn[section].taskChild.count)")
            if listToPutIn[section].taskChild.count == 0 {
                return 1
            }else{
                 return listToPutIn[section].taskChild.count + 1
            }
           
        }else{
            return 1
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return list.count
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if showSwitch == 2{
            if section == 0{
              return "Project"
            }
         return nil
        }else{
            return nil
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return 68
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if showSwitch == 2 {
            if section == 0 {
                return 68
            }
            return 0
        }else{
            return 0
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if showSwitch == 2 {
            if indexPath.row == 0{
                // Project PErform segue
                guard let listToPut = list as? [projectData] else {return}
                let name = listToPut[indexPath.section].nameProject
               let projectToSend =  fetchProjectWithName(nameProject: name)
                performSegue(withIdentifier: segueGoToShow, sender: projectToSend)
                print("Go To Project")
            }else {
                // To Do list
            guard let listToPut = list as? [projectData] else {return}
            let task = listToPut[indexPath.section].taskChild[indexPath.row - 1]
            performSegue(withIdentifier: segueGoToTask, sender: task)
                       print("Go To Task")
            }
        }else{
            if indexPath.row == 0{
                guard let listToPut = list as? [Task] else {return}
                let task = listToPut[indexPath.section]
                performSegue(withIdentifier: segueGoToTask, sender: task)
            }else{

            }
        }
    }


    func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
      
        if showSwitch == 2{
            deleteTaskAndRow(editingStyle: editingStyle, indexPath: indexPath, project: true)
        }else{
            deleteTaskAndRow(editingStyle: editingStyle, indexPath: indexPath, project: false)
        }

    }
    
    func deleteTaskAndRow(editingStyle : UITableViewCell.EditingStyle, indexPath : IndexPath, project : Bool){
        
        if project {
            if indexPath.row == 0 {
                guard var listToDelete = list as? [projectData] else{return}
                let taskToDelete = listToDelete[indexPath.section]
                if editingStyle == .delete {
                    print("Deleted")
                    
                    if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
                        
                        
                       let projectToDelete =  fetchProjectWithName(nameProject: taskToDelete.nameProject)
                        
                        context.delete(projectToDelete!)
                        
                        try? context.save()
                        
                        fetchTaskFromProjectAndDelete(nameProject: taskToDelete.nameProject)
                        
                       listToDelete.remove(at: indexPath.section)
                     
                        list = listToDelete
                        
                         let section = IndexSet(integer: indexPath.section)
                        
                        self.tableViewForTopic.beginUpdates()
//                        self.tableViewForTopic.deleteRows(at: [indexPath], with: .automatic)
                        self.tableViewForTopic.deleteSections(section, with: .automatic)
                        self.tableViewForTopic.endUpdates()
                        
        
                      
                    }
                    
                    
                }
            }else {
                
                guard var listToDelete = list as? [projectData] else{return}
                var taskToDelete = listToDelete[indexPath.section]
                if editingStyle == .delete {
                    print("Deleted")
                    
                    if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
                        
                        context.delete(taskToDelete.taskChild[indexPath.row - 1 ])
                        
                        try? context.save()
                        
                        taskToDelete.taskChild.remove(at: indexPath.row - 1)
                        
                      listToDelete[indexPath.section] = taskToDelete
                      list = listToDelete
       
                        
                        self.tableViewForTopic.beginUpdates()
                        self.tableViewForTopic.deleteRows(at: [indexPath], with: .automatic)
                        self.tableViewForTopic.endUpdates()
                        
                        
                    }
                    
                    
                }
            }
        }else{
            guard let listToDelete = list as? [Task] else{return}
            let taskToDelete = listToDelete[indexPath.section]
            if editingStyle == .delete {
                print("Deleted")
                
                if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
                    
                    context.delete(taskToDelete)
                    
                    try? context.save()
                    
                    self.list.remove(at: indexPath.section)
                    
                      let section = IndexSet(integer: indexPath.section)
                    
                    self.tableViewForTopic.beginUpdates()
                    self.tableViewForTopic.deleteSections(section, with: .automatic)
                    self.tableViewForTopic.endUpdates()
                    
                    
                }
                
                
            }
        }
     
    }

}







extension TopicControllerForTodayAndEtc {
    
    func clearOutEverything(){
        todayList.removeAll()
        upComingList.removeAll()
        inBoxList.removeAll()
        projectList.removeAll()
        projectToPopulate.removeAll()
        

    }
    func appendToeverthing(){
        appendToProjectList()
        appendToProjectToPopulate()
        AppendToTodayAndUpcoming()
    }
    func appendToProjectToPopulate(){
        var i = 0
        
        while i < projectList.count{
            if let projectName =  projectList[i].name,let projectDate = projectList[i].date,let hexString = projectList[i].hexString {
                fetchFromProject(nameProject: projectName, date: projectDate, hexString: hexString)
            }
            i += 1
        }
    }
    
    
    func fetchProjectWithName(nameProject : String) -> Project?{
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Project")
            request.predicate = NSPredicate(format: "%K = %@", "name", "\(nameProject)")
            
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coredataToSearchTaskToDos = try? context.fetch(request) as? [Project] {
                if let theToDos = coredataToSearchTaskToDos {
                    
                    for dataToSearchTask in theToDos {
                        
                        return dataToSearchTask
                        
                    }
                }
            }
        }
        return nil
    }


    func fetchFromProject(nameProject : String,date : String,hexString : String){
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
            request.predicate = NSPredicate(format: "%K = %@", "project", "\(nameProject)")
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coreDataToDos = try? context.fetch(request) as? [Task] {
                if let theToDos = coreDataToDos {
                    
                    for data in theToDos {
                        if data.done {
                            doneTask.append(data)
                        }
                        
                        taskChild.append(data)
                        
                    }
                    projectToPopulate.append(projectData(nameProject: nameProject, date: date, taskChild: taskChild, doneTask: doneTask, hexString: hexString))
                    
                    taskChild.removeAll()
                    doneTask.removeAll()
                    
                }
            }
        }
    }
    
    
    func appendToProjectList(){
        
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Project")
            //            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coreDataToDos = try? context.fetch(request) as? [Project] {
                if let theToDos = coreDataToDos {
                    
                    for data in theToDos {
                        
                        projectList.append(data)
                        
                        
                    }
                    
                    
                }
            }
        }
    }
    
    
    func AppendToTodayAndUpcomingWithCondition(inInBoxList : Bool,inTodayList : Bool, inUpComingList : Bool) {
        let currentDate = dateFormatter.string(from: date)
        
        let currentDateToCompare = dateFormatter.date(from: currentDate)
        
        
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coreDataToDos = try? context.fetch(request) as? [Task] {
                if let theToDos = coreDataToDos {
                    for data in theToDos {
                        if inInBoxList{
                            inBoxList.append(data)
                        }
                        else if inTodayList{
                            if let dateToCompare = dateFormatter.date(from: data.date!) {
                                if (currentDateToCompare?.isEqualTo(dateToCompare))!{
                                    todayList.append(data)
                                }else if (currentDateToCompare?.isSmallerThan(dateToCompare))!{
                                    //                                    upComingList.append(data)
                                }
                            }
                        }else if inUpComingList{
                            
                            if let dateToCompare = dateFormatter.date(from: data.date!) {
                                if (currentDateToCompare?.isEqualTo(dateToCompare))!{
                                    //                                    todayList.append(data)
                                }else if (currentDateToCompare?.isSmallerThan(dateToCompare))!{
                                    upComingList.append(data)
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    func AppendToTodayAndUpcoming() {
        let currentDate = dateFormatter.string(from: date)
        
        let currentDateToCompare = dateFormatter.date(from: currentDate)
        
        
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
            request.sortDescriptors = [NSSortDescriptor(key: "done", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coreDataToDos = try? context.fetch(request) as? [Task] {
                if let theToDos = coreDataToDos {
                    
                    for data in theToDos {
                        
                        inBoxList.append(data)
                        
                        if let dateToCompare = dateFormatter.date(from: data.date!) {
                            if (currentDateToCompare?.isEqualTo(dateToCompare))!{
                                todayList.append(data)
                            }else if (currentDateToCompare?.isSmallerThan(dateToCompare))!{
                                upComingList.append(data)
                            }
                        }
                        
                        
                    }
                    
                    
                }
            }
        }
    }
    
    
    func fetchProjectAndDelete(){
        
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Project")
            //            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coreDataToDos = try? context.fetch(request) as? [Project] {
                if let theToDos = coreDataToDos {
                    
                    for data in theToDos {
                        
                       context.delete(data)
                        
                        try? context.save()
                        
                        
                    }
                    
                    
                }
            }
        }
    }
    
    
    func fetchTaskFromProjectAndDelete(nameProject : String){
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
            request.predicate = NSPredicate(format: "%K = %@", "project", "\(nameProject)")
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coreDataToDos = try? context.fetch(request) as? [Task] {
                if let theToDos = coreDataToDos {
                    
                    for data in theToDos {
                       context.delete(data)
                        
                        try? context.save()
                    }
                 
                }
            }
        }
    }
    
    func fetchAndDelete(inBox : Bool,today : Bool,upComing : Bool){
        let currentDate = dateFormatter.string(from: date)
        
        let currentDateToCompare = dateFormatter.date(from: currentDate)
        
        
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
            
            //            request.sortDescriptors = [NSSortDescriptor(key: "done", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            do {
                let result = try context.fetch(request)
                for data in result as! [Task] {
                    if inBox{
                        context.delete(data)
                        
                        try?  context.save()
                    }else{
                        guard let dateTo = data.date else {return}
                        
                        if let dateToCompare = dateFormatter.date(from: dateTo) {
                            if (currentDateToCompare?.isEqualTo(dateToCompare))!{
                                if today{
                                    context.delete(data)
                                    
                                    try?   context.save()
                                }
                            }else if (currentDateToCompare?.isSmallerThan(dateToCompare))!{
                                if upComing{
                                    context.delete(data)
                                    
                                    try? context.save()
                                }
                                
                            }
                        }
                    }
                }
                
            } catch {
                
                print("Failed")
            }
            

        }
    }
}






extension TopicControllerForTodayAndEtc : EmptyDataSetSource,EmptyDataSetDelegate{
    
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        
        return image
    }
    
    func imageAnimation(forEmptyDataSet scrollView: UIScrollView) -> CAAnimation? {
        return imageAnimation
    }
    
    //    func buttonTitle(forEmptyDataSet scrollView: UIScrollView, for state: UIControl.State) -> NSAttributedString? {
    //        return NSAttributedString(string: "Please add some")
    //    }
    //
    //    func buttonBackgroundImage(forEmptyDataSet scrollView: UIScrollView, for state: UIControl.State) -> UIImage? {
    //        return UIImage(named: "GroupSele")
    //    }
    //
    //
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        
        return NSAttributedString(string: "YOU ARE FREE !!")
    }
    
    func backgroundColor(forEmptyDataSet scrollView: UIScrollView) -> UIColor? {
        return UIColor.white
        
    }
    
    func emptyDataSet(_ scrollView: UIScrollView, didTapButton button: UIButton) {
        print("Tapp Buton")
    }
    
    func emptyDataSet(_ scrollView: UIScrollView, didTapView view: UIView) {
        print("Tappp View")
    }
    
    func verticalOffset(forEmptyDataSet scrollView: UIScrollView) -> CGFloat {
        
        return 15
    }
    
    func spaceHeight(forEmptyDataSet scrollView: UIScrollView) -> CGFloat {
        return 10
    }
}





