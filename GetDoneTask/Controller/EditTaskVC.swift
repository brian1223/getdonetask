//
//  EditTaskVC.swift
//  GetDoneTask
//
//  Created by Thang Thoi Truong on 5/27/19.
//  Copyright © 2019 Thang Thoi Truong. All rights reserved.
//

import UIKit
import  CoreData
import DateTimePicker
import ChameleonFramework

class EditTaskVC: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var taskTextField: UITextField!
    
    @IBOutlet weak var dateLb: UILabel!
    
    @IBOutlet weak var timeLb: UILabel!
    
    @IBOutlet weak var importantLb: UILabel!
    
    @IBOutlet weak var projectLb: UILabel!
 
    
    @IBOutlet weak var importantBtn: UIButton!
    
    @IBOutlet weak var colorShowLb: UILabel!
    
    @IBOutlet weak var pickerProject: UIPickerView!

    
    @IBOutlet weak var changeColorBtn: UIButton!
    
    @IBOutlet weak var dateBtn: UIButton!
    
    @IBOutlet weak var timeBtn: UIButton!
    
    var important = false
    
    
    var previousVCForProject = ShowProjectVC()
    
    var pickerProjectDataArray = [String]()
    
    var picker = DateTimePicker()
    
    var color = UIColor.black
    
    var timeSelected = String()
    
    var dateSelected = String()
    
    var selectedItem = Task()
    
    
    var hexString = String()
    
    var projectSelected = String()
    
  
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        pickerProject.delegate = self
        pickerProject.dataSource = self
        
        
        
      
        taskTextField.text = selectedItem.name
        
        taskTextField.delegate = self
       
        important = selectedItem.important
        if selectedItem.project != ""{
              projectSelected =  selectedItem.project!
        }
       
      
        
        importantButtonState()
        
        taskTextField.becomeFirstResponder()
        
        

    }
    
  
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        pickerProjectDataArray.removeAll()
        fetchProjectName()
        pickerProject.reloadAllComponents()
        
        
        if let dateselected = selectedItem.date,let timeselected = selectedItem.time {
            dateSelected = dateselected
            timeSelected = timeselected
        }
        if dateSelected != "" {
            dateLb.text = " Date : \(dateSelected)"
        }else{
            dateLb.text = " Date : Undefined"
        }
        if timeSelected != "" {
            timeLb.text =  " Time : \(timeSelected )"
        }else {
            timeLb.text =  " Time : Undefined"
        }
        
        
        
        if let hexString = selectedItem.hexString {
            self.hexString = hexString
        }
        
        if hexString == "#ffffff" {
    
            
            colorShowLb.backgroundColor = FlatWhite()
        }else{
           colorShowLb.backgroundColor = UIColor(hexString: hexString)
        }
        
     
        if let int = pickerProjectDataArray.firstIndex(of: projectSelected) {
            pickerProject.selectRow(int, inComponent: 0, animated: true)
        }
       
       
        
        
    }
    
    
    
    
    @IBAction func importantBtnTapped(_ sender: Any) {
        
        if important {
            important = false
            importantButtonState()
        }else{
            important = true
            importantButtonState()
        }
    }
    
    
    
    func importantButtonState(){
        if important == false {
            importantBtn.setTitle("✩", for: .normal)
            
        }else{
            importantBtn.setTitle("★", for: .normal)
        }
        
    }
    
    @IBAction func changeColorTapped(_ sender: Any) {
         performSegue(withIdentifier: "toTagColorVC", sender: self)
    }
    
    
    @IBAction func dateBtnTapped(_ sender: Any) {
        
        performSegue(withIdentifier: "goToCalendar", sender: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToCalendar" {
            guard let chooseDateVC = segue.destination as? ChooseDateAndTimeVC else {return}
            chooseDateVC.color = self.color
            chooseDateVC.delegateSelected  = self
            chooseDateVC.selectedTask = selectedItem
        }
        if segue.identifier == "toTagColorVC"{
              guard let chooseColorVC = segue.destination as? tagColorVC else {return}
            chooseColorVC.delegateTaskColor = self
            chooseColorVC.selectedItem = self.selectedItem
        }
        
    }
    
    @IBAction func timeBtnTapped(_ sender: Any) {
        taskTextField.resignFirstResponder()
        
        createCalendar(isDatePicker: false)
    }
    
    
    
    
    @IBAction func editTapped(_ sender: Any) {
        if let name = selectedItem.name{
               fetchTask(nameTask: name)
        }
        
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func deleteTapped(_ sender: Any) {
        if let name = selectedItem.name{
           deleteTask(nameTask: name)
        }
        
           navigationController?.popViewController(animated: true)
        
    }
    
    
    
    func fetchTask(nameTask : String){
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
            request.predicate = NSPredicate(format: "%K = %@", "name", "\(nameTask)")
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
            
            if let coreDataToDos = try? context.fetch(request) as? [Task] {
                if let theToDos = coreDataToDos {
                    
                    for data in theToDos {
                        if let nameOfTask = taskTextField.text {
                            
                           
                                data.name = nameOfTask
                                data.date = dateSelected
                                data.time = timeSelected
                                data.important = important
                                data.hexString = hexString
                                data.done = false
                                data.project = projectSelected
                                
                            
                            
                            
                        }
                        
                        try? context.save()
                     
                        
                    }
           
                    
                }
            }
        }
    }
    
    func deleteTask(nameTask : String){
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
        request.predicate = NSPredicate(format: "%K = %@", "name", "\(nameTask)")
        request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
        request.returnsObjectsAsFaults = false
        
        if let coreDataToDos = try? context.fetch(request) as? [Task] {
        if let theToDos = coreDataToDos {
        
            for data in theToDos {
                
                context.delete(data)
                
                try? context.save()
                
            }
            
            
            }
            }
        }
    }
    
    func fetchProjectName() {
        pickerProjectDataArray.append("none")
        
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            
            if let coreDataToDos = try? context.fetch(Project.fetchRequest()) as? [Project] {
                if let theToDos = coreDataToDos {
                    
                    for data in theToDos {
                        print(data)
                        if let projectNameToAppend = data.name {
                            pickerProjectDataArray.append(projectNameToAppend)
                            
                        }
                    }
                    
                    
                }
                
                
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
    }
    
    
    func createCalendar(isDatePicker : Bool){
        let min = Date()
        //            .addingTimeInterval(-60 * 60 * 24 * 4)
        let max = Date().addingTimeInterval(480 * 60 * 24 * 4)
        picker = DateTimePicker.create(minimumDate: min, maximumDate: max)
        
        //                picker.dateFormat = "hh:mm aa dd/MM/YYYY"
        if isDatePicker {
            picker.dateFormat = "dd/MM/YYYY"
            picker.isDatePickerOnly = true
        }else{
            picker.dateFormat = "h:mm a"
            picker.is12HourFormat = true
            picker.todayButtonTitle = ""
            picker.isTimePickerOnly = true
        }
        
        
        picker.includeMonth = true // if true the month shows at bottom of date cell
        picker.highlightColor = color
        picker.darkColor = UIColor.darkGray
        picker.doneButtonTitle = "Save time"
        picker.doneBackgroundColor = color
        
        if isDatePicker {
            picker.completionHandler = { date in
                
                let formatter = DateFormatter()
                formatter.dateFormat = "dd/MM/YYYY"
                self.dateLb.text = formatter.string(from: date)
                
            }
        }else{
            picker.completionHandler = { date in
                
                let formatter = DateFormatter()
                formatter.dateFormat = "h:mm a"
                formatter.amSymbol  = "AM"
                formatter.pmSymbol = "PM"
                
                self.timeSelected = formatter.string(from: date)
                self.timeLb.text = "Time : \(self.timeSelected)"
                
            }
        }
        
        //         add tap gesture to dismiss
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissDateAndTime))
        tap.cancelsTouchesInView = true
        tap.numberOfTapsRequired = 2
        self.picker.addGestureRecognizer(tap)
        
        picker.delegate = self
        picker.show()
    }
    
    
    @objc func dismissDateAndTime(){
        picker.dismissView()
    }
    
}


extension EditTaskVC : DateTimePickerDelegate{
    func dateTimePicker(_ picker: DateTimePicker, didSelectDate: Date) {
        
    }
    
    
}


extension EditTaskVC : UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerProjectDataArray.count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let attributedString = NSAttributedString(string: pickerProjectDataArray[row], attributes: [NSAttributedString.Key.foregroundColor : color])
        return attributedString
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row == 0 {
            projectSelected = ""
        }else{
            projectSelected = pickerProjectDataArray[row]
        }
        
    }
}



extension EditTaskVC : MySelectedItemDelegateProtocol{
    func sendItem(myData: Task) {
        self.selectedItem = myData
    }
    
    
}

extension EditTaskVC : MyTaskColorSendingProtocol{
    func sendTask(myData: Task) {
        self.selectedItem = myData
    }
    
}

